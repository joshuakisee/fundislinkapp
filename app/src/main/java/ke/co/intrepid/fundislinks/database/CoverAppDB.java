package ke.co.intrepid.fundislinks.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ke.co.intrepid.fundislinks.log.L;

/**
 * Created by Clifford Owino on 9/14/2016.
 */
public class CoverAppDB {


    private CoverAppDBHelper mHelper;
    private SQLiteDatabase mDatabase;

    public CoverAppDB(Context context){
        mHelper = new CoverAppDBHelper(context);
        mDatabase = mHelper.getWritableDatabase();
    }


    private static class CoverAppDBHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME                     =               "DoorstepDB";
        private static final String COUNT_TABLE_ROWS = "SELECT COUNT(*) FROM ";
        private static final int DATABASE_VERSION = 1;
        private Context mContext;

        public CoverAppDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.mContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL("CREATE_TABLE_SQL");
            } catch (SQLException e) {
                L.m("SQLException onCreate " + e);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL("DROP_TABLE_SQL");
                onCreate(db);

            } catch (SQLException e) {
                L.m("SQLException onUpgrade " + e);
            }
        }

    }
}
