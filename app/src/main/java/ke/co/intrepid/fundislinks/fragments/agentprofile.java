package ke.co.intrepid.fundislinks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.network.CustomVolleyRequest;
import ke.co.intrepid.fundislinks.R;

public class agentprofile extends Fragment {
    NetworkImageView agentprofilepic;
    TextView profileagentid,agentname, profileagentbussinessname, profileagentlocation, profileagentdescription;
    private ImageLoader imageLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agentprofile, container, false);
// Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("FundisLink Fundi");


        return inflater.inflate(R.layout.fragment_agentprofile, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        agentprofilepic=(NetworkImageView) view.findViewById(R.id.thumbnail_agents_profile);
        profileagentid=(TextView) view.findViewById(R.id.profile_id_description);
        agentname=(TextView) view.findViewById(R.id.profile_name_description);
        profileagentbussinessname=(TextView) view.findViewById(R.id.profile_bsname_description);
        profileagentlocation=(TextView) view.findViewById(R.id.profile_location_description);
        profileagentdescription=(TextView) view.findViewById(R.id.profile_agent_description);

       // String value=name;

        assign();

        Button view_agents=(Button) view.findViewById(R.id.view_agents);
        view_agents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm=getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new fundi_agents())
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    public void assign(){
        fundi_availabe_agents agentid=new fundi_availabe_agents();

        String agent_name=agentid.agentName;
        String agent_id=agentid.agent;
        String bsname=agentid.name;
        String location=agentid.location;
        String adescription=agentid.description;

        loadImage();

        agentname.setText(agent_name);
        profileagentid.setText(agent_id);
        profileagentbussinessname.setText(bsname);
        profileagentlocation.setText(location);
        profileagentdescription.setText(adescription);


    }

    private void loadImage(){
        fundi_availabe_agents image=new fundi_availabe_agents();
        String agentimg=image.agentimage;
        String url = agentimg;//"http://dashboard.fundislink.com/image_uploads/agents/f9d63f481765a59695e14afecaf694ea.png";

        imageLoader = CustomVolleyRequest.getInstance(getActivity().getApplicationContext())
                .getImageLoader();
        imageLoader.get(url, ImageLoader.getImageListener(agentprofilepic,
                R.drawable.add_image, android.R.drawable
                        .ic_dialog_alert));
        agentprofilepic.setImageUrl(url, imageLoader);
    }

}
