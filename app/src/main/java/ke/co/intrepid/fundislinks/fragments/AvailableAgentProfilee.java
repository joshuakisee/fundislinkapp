package ke.co.intrepid.fundislinks.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import ke.co.intrepid.fundislinks.activities.Booking;
import ke.co.intrepid.fundislinks.activities.LoginActivity;
import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;
import ke.co.intrepid.fundislinks.network.CustomVolleyRequest;

public class AvailableAgentProfilee extends Fragment {
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String KEY_LOGIN_PHONE_NUMBER="username";
    public static final String KEY_LOGIN_PASSWORD="password";
    NetworkImageView availableagentprofilepic;
    TextView availableprofileagentid,availableagentname, availableprofileagentbussinessname, availableprofileagentlocation, availableprofileagentdescription,rating;
    private List<Image> images;
    private Context mContext;
    private ImageLoader imageLoader;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("FundisLink Agents");

        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_available_agent_profilee, container, false);

        return inflater.inflate(R.layout.fragment_available_agent_profilee, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        availableagentprofilepic=(NetworkImageView) view.findViewById(R.id.thumbnail_available_agents_profile);
        availableprofileagentid=(TextView) view.findViewById(R.id.profile_available_id_description);
        availableagentname=(TextView) view.findViewById(R.id.profile_available_name_description);
        //availableprofileagentbussinessname=(TextView) view.findViewById(R.id.profile_available_bsname_description);
        availableprofileagentlocation=(TextView) view.findViewById(R.id.profile_available_location_description);
        availableprofileagentdescription=(TextView) view.findViewById(R.id.profile_available_agent_description);
        rating=(TextView) view.findViewById(R.id.profile_rating_description);

        assigning();


        Button book=(Button) view.findViewById(R.id.fundi_book);
        Button call=(Button) view .findViewById(R.id.call_agent);
        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedpref();

            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MakeCall();
            }
        });
    }

    public void sharedpref()
    {
        SharedPreferences editor = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        String useid = editor.getString("KEY_USERID", "No id");
        String usename = editor.getString("KEY_USERNAME", "oops no name");
        String phone=editor.getString(KEY_LOGIN_PHONE_NUMBER,null);
        String pssword=editor.getString(KEY_LOGIN_PASSWORD,null);

        if (phone != null && pssword != null) {
            startActivity(new Intent(AppController.getInstance(), Booking.class));
        }
        else {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        }

    }

    public void assigning(){
        fundi_agents fundis=new fundi_agents();
        //Toast.makeText(getActivity(), fundis.pic, Toast.LENGTH_SHORT).show();
        String fundi_name=fundis.availableName;
        String fundi_id=fundis.fundiId;
        String fundi_location=fundis.availableLocation;
        String Fundi_description=fundis.availabledescription;
        String rate=fundis.rating;

        /** decode the shared image**/
        SharedPreferences sharepic = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String previouslyEncodedImage = sharepic.getString("image_data", "");
        String Phone=sharepic.getString("phone","");
       // Toast.makeText(getActivity(), previouslyEncodedImage, Toast.LENGTH_LONG).show();

        //if( !previouslyEncodedImage.equalsIgnoreCase("") ) {
        //if(previouslyEncodedImage != ("") ) {
//            byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
//            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
//            availableagentprofilepic.setImageBitmap(bitmap);

//        }
//        else{
//            Toast.makeText(getActivity(), previouslyEncodedImage, Toast.LENGTH_LONG).show();
//        }
        loadImage();
        availableagentname.setText(fundi_name);
        availableprofileagentid.setText(fundi_id);
        rating.setText(rate);
        availableprofileagentlocation.setText(fundi_location);
        availableprofileagentdescription.setText(Fundi_description);


    }
    public void MakeCall(){
        SharedPreferences sharepic = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String Phone=sharepic.getString("phone","");

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" +Phone));
        startActivity(intent);
    }

    private void loadImage(){

        fundi_agents f=new fundi_agents();
        String img=f.pic;
        //Toast.makeText(getActivity(), img, Toast.LENGTH_SHORT).show();
        String urll =img;// "http://dashboard.fundislink.com/image_uploads/agents/f9d63f481765a59695e14afecaf694ea.png";
//        if(url.equals("")){
//            Toast.makeText(this,"Please enter a URL",Toast.LENGTH_LONG).show();
//            return;
//        }

        imageLoader = CustomVolleyRequest.getInstance(getActivity().getApplicationContext())
                .getImageLoader();
        imageLoader.get(urll, ImageLoader.getImageListener(availableagentprofilepic,
                R.drawable.add_image, android.R.drawable
                        .ic_dialog_alert));
        availableagentprofilepic.setImageUrl(urll, imageLoader);
    }

}
