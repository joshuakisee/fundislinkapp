package ke.co.intrepid.fundislinks.utility;

import org.json.JSONObject;

/**
 * Created by Clifford Owino on 9/14/2016.
 */
public class ParseUtil {
    public static boolean contains(JSONObject jsonObject, String key) {
        return jsonObject != null && !key.matches("false") && jsonObject.has(key) && !jsonObject.isNull(key) ? true : false;
    }
}
