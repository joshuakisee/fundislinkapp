package ke.co.intrepid.fundislinks.adapter;

import android.content.Context;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;


/**
 * Created by joshua on 10/31/2016.
 */
public class AgentAdapter extends RecyclerView.Adapter<AgentAdapter.MyViewHolder> {

    private List<Image> images;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView agentid, agentbussinessname, agentlocation, agentdescription;
        public ImageView thumbnail,thumbnail_available_agents_profile;

        public MyViewHolder(View view) {
            super(view);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail_agents);
            agentid = (TextView) view.findViewById(R.id.id_fundiagent);
            agentbussinessname = (TextView) view.findViewById(R.id.business_name);
            agentlocation = (TextView) view.findViewById(R.id.agent_location);
            agentdescription = (TextView) view.findViewById(R.id.Agent_description);
            //thumbnail_available_agents_profile=(ImageView) view.findViewById(R.id.thumbnail_available_agents_profile);

        }
    }


    public AgentAdapter(Context context, List<Image> images) {
        mContext = context;
        this.images = images;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_thumbnail_agents, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Image image = images.get(position);

        Glide.with(mContext).load(image.getAgentimage())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail);
//        Glide.with(mContext).load(image.getAgentimage())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.thumbnail_available_agents_profile);

        holder.agentid.setText(image.getAgentid());
        holder.agentbussinessname.setText(image.getAgentname());
        holder.agentlocation.setText(image.getAgentlocation());
        holder.agentdescription.setText(image.getAgentdescription());

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private AgentAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final AgentAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
