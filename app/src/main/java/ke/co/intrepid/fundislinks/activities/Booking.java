package ke.co.intrepid.fundislinks.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.fragments.fundi_agents;
import ke.co.intrepid.fundislinks.fragments.fundi_availabe_agents;
import ke.co.intrepid.fundislinks.fragments.homeweb;
import ke.co.intrepid.fundislinks.R;

public class Booking extends AppCompatActivity implements View.OnClickListener {
    //$speciality_id, $created_by, $description, $fundi_id, $agent_id, $date_booked, $job_image);

    private static final String book_url="http://dashboard.fundislink.com/Json/Json/kazi";
    public static final String KEY_USER_ID="user_id";
    public static final String KEY_DATE="date_booked";
    public static final String KEY_SPECIALITY_ID="speciality_id";
    public static final String KEY_DESCTRIPTION="description";
    public static  final String KEY_FUNDI_ID="fundi_id";
    public static final String KEY_AGENT_ID="agent_id";
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String KEY_USERID="useid";
    public static final String KEY_USERNAME="usename";
    public static final String KEY_LOGIN_PHONE_NUMBER="username";
    public static final String KEY_LOGIN_PASSWORD="password";

    ImageView book_image;
    EditText book_date,jobdescription;
    Button book_job;
    TextView specility_id,agents_id,fundis_id,users_id;

    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView;
    int TAKE_PHOTO_CODE = 0;
    public static int count = 0;
    private static int RESULT_LOAD_IMAGE = 1;
    private EditText mycalendar;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        book_image=(ImageView) findViewById(R.id.theimage);
        book_date=(EditText) findViewById(R.id.mydate);
        jobdescription=(EditText) findViewById(R.id.descriptive);
        users_id=(TextView) findViewById(R.id.user_id);
        specility_id=(TextView) findViewById(R.id.speciality_id);
        fundis_id=(TextView) findViewById(R.id.fundi_id);
        agents_id=(TextView) findViewById(R.id.agent_id);

        book_job=(Button) findViewById(R.id.bookAgent);
        book_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                book_job();
            }
        });

        /**start getting the ids**/

        homeweb splity=new homeweb();
        //Fundi_services s=new Fundi_services();
        fundi_availabe_agents fundisid= new fundi_availabe_agents();
        fundi_agents agent=new fundi_agents();
//        LoginActivity i=new LoginActivity();
        sharedpref();
        specility_id.setText(splity.test);
        fundis_id.setText(fundisid.agent);
        agents_id.setText(agent.fundiId);

        /**end getting ids**/

        //mycalendar=(EditText) findViewById(R.id.mydate);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        findViewsById();
        setDateTimeField();


        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();


                this.imageView = (ImageView)this.findViewById(R.id.theimage);
                Button photoButton = (Button) this.findViewById(R.id.camera);
                photoButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                });

        Button buttonLoadImage = (Button) findViewById(R.id.gallery);
        buttonLoadImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

    }
    /**set date start**/
    private void findViewsById() {
        mycalendar = (EditText) findViewById(R.id.mydate);
        mycalendar.setInputType(InputType.TYPE_NULL);
        mycalendar.requestFocus();

    }
    private void setDateTimeField() {
        mycalendar.setOnClickListener(this);
        //toDateEtxt.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mycalendar.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View view) {
        if(view == mycalendar) {
            fromDatePickerDialog.show();
        }
    }
    /**set date end**/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
//            Log.d("CameraDemo", "Pic saved");
//        }

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
        }

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.theimage);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }
    }

    public void sharedpref()
    {
        SharedPreferences editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        String useid = editor.getString("KEY_USERID", "No id");
        String usename = editor.getString("KEY_USERNAME", "oops no name");
        String uname=editor.getString(KEY_LOGIN_PHONE_NUMBER,null);
        String pssword=editor.getString(KEY_LOGIN_PASSWORD,null);
        users_id.setText(useid);
//        Toast.makeText(Booking.this,uname,Toast.LENGTH_LONG).show();
//        Toast.makeText(Booking.this,pssword,Toast.LENGTH_LONG).show();

    }

    /**start save to database**/
    private void book_job()
    {
        final String user_id=users_id.getText().toString().trim();
        final String date_booked=book_date.getText().toString().trim();
        final String speciality_id=specility_id.getText().toString().trim();
        final String description=jobdescription.getText().toString().trim();
        final String fundi_id= fundis_id.getText().toString().trim();
        final String agent_id=agents_id.getText().toString().trim();


if(description.equals("")) {
    jobdescription.setError( "Description is required!" );
    }
else if(date_booked.equals(""))
        {
    book_date.setError( "Date is required!" );
    }
        else {
    final ProgressDialog progress = ProgressDialog.show(this,"Loading", "Please wait...");


    StringRequest stringRequest = new StringRequest(Request.Method.POST, book_url,
            new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    progress.dismiss();
                    if (response.trim().equals("{\"status\":\"300\"}")) {
                        Toast.makeText(Booking.this, "Balance too little to book job", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(AppController.getInstance(), MainActivity.class));

                    }
                    else if (response.trim().equals("{\"status\":\"200\"}")) {
                        Toast.makeText(Booking.this, "Successfully booked fundi", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(AppController.getInstance(), MainActivity.class));

                    } else {
                        //Toast.makeText(Booking.this, response, Toast.LENGTH_LONG).show();
                        Toast.makeText(Booking.this, "Sorry we couldn't complete Transaction... Try again", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(AppController.getInstance(), MainActivity.class));

                    }

                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   // Toast.makeText(Booking.this, error.toString(), Toast.LENGTH_LONG).show();
                    //Toast.makeText(Booking.this, error.toString(), Toast.LENGTH_LONG).show();
                    Toast.makeText(Booking.this, "Sorry we couldn't complete Transaction... Try again", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(AppController.getInstance(), MainActivity.class));

                }
            }) {

        @Override
        protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<String, String>();
            params.put(KEY_USER_ID, user_id);
            params.put(KEY_DATE, date_booked);
            params.put(KEY_SPECIALITY_ID, speciality_id);
            params.put(KEY_DESCTRIPTION, description);
            params.put(KEY_FUNDI_ID, fundi_id);
            params.put(KEY_AGENT_ID, agent_id);

            return params;
        }

    };

    RequestQueue requestQueue = Volley.newRequestQueue(this);
    requestQueue.add(stringRequest);
}
    }
    /**end save to database**/
}
