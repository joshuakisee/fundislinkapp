package ke.co.intrepid.fundislinks.pojo;

/**
 * Created by Clifford Owino on 9/14/2016.
 */
public class Keys {
    public interface keys{
        public static final int MY_SOCKET_TIMEOUT_MS = 10000;
        public static final int MY_DEFAULT_MAX_RETRIES = 3;
        public static final int SPLASH_DISPLAY_TIMER = 700;
        public static final String EMAIL="email";
        public static final String FNAME="fname";
        public static final String LNAME="lname";
        public static final String GENDER="gender";
        public static final String NOTIFICATION ="Notification";
        public static final String MALE ="M";
        public static final String FEMALE ="F";
        public static final String TAG_STATUS = "status";
        public static final String TAG_DATA = "data";
        public static final String TAG_PRODUCTS = "products";

//        public static final String KEY_NAME = "name";
//        public static final String KEY_IMAGE_URL = "image_url";
//        public static final String KEY_ID = "id";
//        public static final String KEY_URL="http://dashboard.fundislink.com/Json/Json/speciality_category";
//        public static final String KEY_JSON_ARRAY="results";
    }
}
