package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import ke.co.intrepid.fundislinks.R;
import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.app.AppController;

public class EmergencyBook extends Fragment {
    TextView UserId ,subid;
    EditText description;
    private static final String subcategory_url="http://dashboard.fundislink.com/Json/json/emergency";

    public static final String MyPREFERENCES = "MyPrefs";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public EmergencyBook() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EmergencyBook newInstance(String param1, String param2) {
        EmergencyBook fragment = new EmergencyBook();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_emergency_book, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UserId=(TextView) view.findViewById(R.id.userid);
        description=(EditText) view.findViewById(R.id.mdescriptive);
        subid=(TextView) view.findViewById(R.id.sub_categories_id);


        sharedpref();
        Button bookEmergency=(Button) view.findViewById(R.id.bookEmergency);
        bookEmergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                book_job();
            }
        });

    }


    public void sharedpref()
    {
        SharedPreferences editor = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_APPEND);

        String useid = editor.getString("KEY_USERID", "");
        String subcatid = editor.getString("SUBCATEGORY", "");

        UserId.setText(useid);
        subid.setText(subcatid);
//        Toast.makeText(Booking.this,uname,Toast.LENGTH_LONG).show();
//        Toast.makeText(Booking.this,pssword,Toast.LENGTH_LONG).show();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**start save to database**/
    private void book_job()
    {

        final String user_id=UserId.getText().toString().trim();
        final String sub_categories_id=subid.getText().toString().trim();
        final String desc=description.getText().toString().trim();

        if(desc.equals("")) {
            description.setError( "Description is required!" );
        }
//        else if(date_booked.equals(""))
//        {
//            book_date.setError( "Date is required!" );
//        }
        else {
            final ProgressDialog progress = ProgressDialog.show(getActivity(),"Loading", "Please wait...");


            StringRequest stringRequest = new StringRequest(Request.Method.POST, subcategory_url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            progress.dismiss();
                            if (response.trim().equals("{\"status\":\"300\"}")) {
                                Toast.makeText(getActivity(), "Balance too little to book job", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(AppController.getInstance(), MainActivity.class));

                            }
                            else if (response.trim().equals("{\"status\":\"200\"}")) {
                                Toast.makeText(getActivity(), "Successfully booked fundi", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(AppController.getInstance(), MainActivity.class));

                            } else {
                                Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(AppController.getInstance(), MainActivity.class));

                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                            //Toast.makeText(Booking.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("created_by", user_id);
                    params.put("speciality_id", sub_categories_id);
                    params.put("description", desc);


                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

    }
    /**end save to database**/
}}

