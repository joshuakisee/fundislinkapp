package ke.co.intrepid.fundislinks.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ke.co.intrepid.fundislinks.R;


public class ProfileFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    CardView paints, computer_services,autoCare,welding,Security,Plumber,Electrical;


    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (container != null) {
            container.removeAllViews();
        }
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
//        //computer_services = (CardView) view.findViewById(R.id.computing);
//        computer_services.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                FragmentManager fm = getFragmentManager();
////                fm.beginTransaction().replace(R.id.content_frame, new -computer_services()).commit();
//
//            }
//
//        });
//        super.onViewCreated(view, savedInstanceState);
//        //paints=(CardView) view.findViewById(R.id.painting);
//        paints.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
//                FragmentManager fm=getFragmentManager();
//                fm.beginTransaction().replace(R.id.content_frame, new painting_services()).commit();
//            }
//        });
//
//        super.onViewCreated(view, savedInstanceState);
//        //autoCare=(CardView) view.findViewById(R.id.autoCare);
//        autoCare.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
////                FragmentManager fm=getFragmentManager();
////                fm.beginTransaction().replace(R.id.content_frame, new autocare_services()).commit();
//            }
//        });
//
//        super.onViewCreated(view, savedInstanceState);
//        //welding=(CardView) view.findViewById(R.id.welding);
//        welding.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
////                FragmentManager fm=getFragmentManager();
////                fm.beginTransaction().replace(R.id.content_frame, new welding_services()).commit();
//            }
//        });
//
//        super.onViewCreated(view, savedInstanceState);
//        //Security=(CardView) view.findViewById(R.id.Security);
//        Security.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
////                FragmentManager fm=getFragmentManager();
////                fm.beginTransaction().replace(R.id.content_frame, new security_services()).commit();
//            }
//        });
//
//        super.onViewCreated(view, savedInstanceState);
//        //Plumber=(CardView) view.findViewById(R.id.Plumber);
//        Plumber.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
////                FragmentManager fm=getFragmentManager();
////                fm.beginTransaction().replace(R.id.content_frame, new plumbers()).commit();
//            }
//        });
//
//        super.onViewCreated(view, savedInstanceState);
//        //Electrical=(CardView) view.findViewById(R.id.Electrical);
//        Electrical.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
////                FragmentManager fm=getFragmentManager();
////                fm.beginTransaction().replace(R.id.content_frame, new ELECTRICAL()).commit();
//            }
//        });
    }

}
