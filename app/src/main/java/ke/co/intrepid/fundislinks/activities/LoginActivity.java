package ke.co.intrepid.fundislinks.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.fragments.EmergencyBook;
import ke.co.intrepid.fundislinks.fragments.FundiProcess;
import ke.co.intrepid.fundislinks.utility.ParseUtil;
import ke.co.intrepid.fundislinks.R;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    public static boolean contains(JSONObject jsonObject, String key) {
        return jsonObject != null && !key.matches("false") && jsonObject.has(key) && !jsonObject.isNull(key) ? true : false;
    }
    /**rest password**/
    private static final String CHANGE_URL = "http://dashboard.fundislink.com/Fundi/auto_update_client";
    private static final String RESET_URL = "http://dashboard.fundislink.com/Fundi/update_client";
    /**start sign_up keys**/
    private static final String REGISTER_URL = "http://dashboard.fundislink.com/Fundi/create_client";
    public static final String KEY_LOGIN_FNAME="names";
    public static final String KEY_LOGIN_LNAME="loginlname";
    public static final String KEY_EMAIL="email";
    public static final String KEY_PHONE="phone";
    public static final String KEY_PASSWORD="password";
    public static final String KEY_LOCATION="location";
    public static final String KEY_CURENTPASS="passwords";
    public static final String KEY_NEWPASS="passwordnew";
    public static final String KEY_ID="id";


    /**end sign_up keys**/

    /**start login keys**/
    private static final String LOGIN_URL = "http://dashboard.fundislink.com/Fundi/user";
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String KEY_LOGIN_PHONE_NUMBER="username";
    public static final String KEY_LOGIN_PASSWORD="password";
    public static final String KEY_USERID="passwords";
    public static final String KEY_USERNAME="passwordnew";

    //SharedPreferences sharedpreferences;
    /**end login keys**/

    LinearLayout login_name_layout, terms_layout;
    TextInputLayout login_layout_fname, login_layout_lname, login_layout_email, login_layout_phone,login_layout_location, login_layout_password, login_layout_password_repeat,login_layout_currentPassword,login_layout_newPassword;
    TextInputEditText login_fname, login_lname, login_email, login_phone, login_location, login_password, login_password_repeat, login_password_currentPassword,login_password_newPassword;
    CheckBox check_terms_conditions;
    TextView text_terms_conditions;
    AppCompatButton login_to_login_view, login_to_login_view_pass, login_but_signup, login_but_login, login_to_forgot_pass_view,login_to_signup_view, login_but_reset_pass,login_to_change_pass_view,change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //GetFramentIds();


        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        login_layout_fname = (TextInputLayout) findViewById(R.id.login_layout_fname);
        login_layout_lname = (TextInputLayout) findViewById(R.id.login_layout_lname);
        login_layout_email = (TextInputLayout) findViewById(R.id.login_layout_email);
        login_layout_phone = (TextInputLayout) findViewById(R.id.login_layout_phone);
        login_layout_location=(TextInputLayout) findViewById(R.id.login_layout_location);
        login_layout_password = (TextInputLayout) findViewById(R.id.login_layout_password);
        login_layout_password_repeat = (TextInputLayout) findViewById(R.id.login_layout_password_repeat);
        login_layout_currentPassword = (TextInputLayout) findViewById(R.id.login_layout_currentPassword);
        login_layout_newPassword = (TextInputLayout) findViewById(R.id.login_layout_newPassword);

        login_fname = (TextInputEditText) findViewById(R.id.login_fname);
        login_lname = (TextInputEditText) findViewById(R.id.login_lname);
        login_email = (TextInputEditText) findViewById(R.id.login_email);
        login_phone = (TextInputEditText) findViewById(R.id.login_phone);
        login_location=(TextInputEditText) findViewById(R.id.login_location);
        login_password = (TextInputEditText) findViewById(R.id.login_password);
        login_password_repeat = (TextInputEditText) findViewById(R.id.login_password_repeat);
        login_password_currentPassword = (TextInputEditText) findViewById(R.id.login_password_currentPassword);
        login_password_newPassword = (TextInputEditText) findViewById(R.id.login_password_newPassword);

        login_to_login_view = (AppCompatButton) findViewById(R.id.login_to_login_view);
        login_but_signup = (AppCompatButton) findViewById(R.id.login_but_signup);
        login_to_login_view_pass = (AppCompatButton) findViewById(R.id.login_to_login_view_pass);
        login_but_login = (AppCompatButton) findViewById(R.id.login_but_login);
        login_to_forgot_pass_view = (AppCompatButton) findViewById(R.id.login_to_forgot_pass_view);
        login_to_signup_view = (AppCompatButton) findViewById(R.id.login_to_signup_view);
        login_but_reset_pass = (AppCompatButton) findViewById(R.id.login_but_reset_pass);
        login_to_change_pass_view = (AppCompatButton) findViewById(R.id.login_to_change_pass_view);
        change_password=(AppCompatButton) findViewById(R.id.change_password);

        login_to_login_view.setOnClickListener(this);
        login_but_signup.setOnClickListener(this);
        login_to_login_view_pass.setOnClickListener(this);
        login_but_login.setOnClickListener(this);
        login_to_forgot_pass_view.setOnClickListener(this);
        login_to_signup_view.setOnClickListener(this);
        login_but_reset_pass.setOnClickListener(this);
        login_to_change_pass_view.setOnClickListener(this);
        change_password.setOnClickListener(this);

        terms_layout = (LinearLayout) findViewById(R.id.terms_layout);
        login_name_layout = (LinearLayout) findViewById(R.id.login_name_layout);

        check_terms_conditions = (CheckBox) findViewById(R.id.check_terms_conditions);

        if (check_terms_conditions != null) {
            check_terms_conditions.setChecked(false);
            check_terms_conditions.setOnClickListener(this);
        }
        text_terms_conditions = (TextView) findViewById(R.id.text_terms_conditions);
        text_terms_conditions.setText(Html.fromHtml("  <i>Terms and conditions</i>"));

        if (text_terms_conditions != null) {
            text_terms_conditions.setOnClickListener(this);
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.text_terms_conditions:
                //startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(getString(R.string.site_url) + "/terms")));
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ft.replace(R.id.content_frame, new TermsandCondions(), getString(R.string.fragment_tag_home));
//                ft.commit();
                break;

            case R.id.login_to_login_view:
                toLoginView();
                break;

            case R.id.login_but_signup:
//                sharedpref();
                final String names = login_fname.getText().toString().trim();
                final String location = login_location.getText().toString().trim();
                final String phone = login_phone.getText().toString().trim();
                final String password = login_password.getText().toString().trim();
//                login_password = (TextInputEditText) findViewById(R.id.login_password);
//                login_password_repeat = (TextInputEditText) findViewById(R.id.login_password_repeat);


                if(names.equals("")) {
                    login_fname.setError( "Required!" );
                }
                else if(location.equals(""))
                {
                    login_location.setError( "Required!" );
                }
                else if(phone.equals(""))
                {
                    login_phone.setError( "Required!" );
                }
                else if(password.equals(""))
                {
                    login_password.setError( "Required!" );
                }
//                else if(login_password!=login_password_repeat)
//                {
//                    login_password_repeat.setError( "don't match!" );
//                }
                else {
                    signup();
                }
                break;

            case R.id.login_to_login_view_pass:
                toLoginView();
                break;

            case R.id.login_but_login:
                networkLogin();
                break;

            case R.id.login_to_forgot_pass_view:
                toForgotPass();
                break;

            case R.id.login_to_signup_view:
                toSignUpView();
                break;
            case R.id.login_to_change_pass_view:
                tochangePass();
                break;
            case R.id.change_password:
                ChangePassword();
                break;

            case R.id.login_but_reset_pass:
                //toLoginView();
                //Toast.makeText(LoginActivity.this, "enter new password",Toast.LENGTH_LONG).show();
                SharedPreferences editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                String useid = editor.getString("KEY_USERID", "");

                if (useid == "") {
                    final String email = login_email.getText().toString().trim();
                    final String phonee = login_phone.getText().toString().trim();

                    toLoginResetPassword();

                    if(email.equals("")) {
                        login_email.setError( "Required!" );
                    }
                    else if(phonee.equals(""))
                    {
                        login_phone.setError( "Required!" );
                    }
                    else {
                        AutoGeneratePassword();
                        //Toast.makeText(LoginActivity.this, "yoouve to login", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "you've logged",Toast.LENGTH_LONG).show();

                }

                break;
        }

    }


    private void toSignUpView() {
        login_to_login_view.setVisibility(View.VISIBLE);
        login_but_signup.setVisibility(View.VISIBLE);
        login_to_login_view_pass.setVisibility(View.GONE);
        login_but_login.setVisibility(View.GONE);
        login_to_forgot_pass_view.setVisibility(View.GONE);
        login_to_signup_view.setVisibility(View.GONE);
        login_but_reset_pass.setVisibility(View.GONE);
        terms_layout.setVisibility(View.VISIBLE);
        login_name_layout.setVisibility(View.VISIBLE);
        change_password.setVisibility(View.GONE);

        login_layout_currentPassword.setVisibility(View.GONE);
        login_layout_newPassword.setVisibility(View.GONE);
        login_layout_email.setErrorEnabled(false);
        login_layout_email.setVisibility(View.VISIBLE);
        login_layout_phone.setVisibility(View.VISIBLE);
        login_layout_phone.setErrorEnabled(false);
        login_layout_password.setVisibility(View.VISIBLE);
        login_layout_password.setErrorEnabled(false);
        login_layout_password_repeat.setVisibility(View.VISIBLE);
        login_layout_password_repeat.setErrorEnabled(false);
        login_layout_fname.setErrorEnabled(false);
        login_layout_lname.setErrorEnabled(false);
        login_layout_location.setVisibility(View.VISIBLE);
        login_layout_location.setErrorEnabled(false);


    }

    private void toForgotPass() {
        login_to_login_view.setVisibility(View.GONE);
        login_but_signup.setVisibility(View.GONE);
        login_to_login_view_pass.setVisibility(View.VISIBLE);
        login_but_login.setVisibility(View.GONE);
        login_to_forgot_pass_view.setVisibility(View.GONE);
        login_to_signup_view.setVisibility(View.GONE);
        login_but_reset_pass.setVisibility(View.VISIBLE);
        terms_layout.setVisibility(View.GONE);
        login_name_layout.setVisibility(View.GONE);
        change_password.setVisibility(View.GONE);

        login_layout_currentPassword.setVisibility(View.GONE);
        login_layout_newPassword.setVisibility(View.GONE);
        login_layout_email.setErrorEnabled(false);
        login_layout_email.setVisibility(View.VISIBLE);
        login_layout_phone.setVisibility(View.VISIBLE);
        login_layout_phone.setErrorEnabled(false);
        login_layout_password.setVisibility(View.GONE);
        login_layout_password.setErrorEnabled(false);
        login_layout_password_repeat.setVisibility(View.GONE);
        login_layout_password_repeat.setErrorEnabled(false);
        login_layout_fname.setErrorEnabled(false);
        login_layout_lname.setErrorEnabled(false);
        login_layout_location.setVisibility(View.GONE);
        login_layout_location.setErrorEnabled(false);
    }

    private void tochangePass() {
        login_to_login_view.setVisibility(View.GONE);
        login_but_signup.setVisibility(View.GONE);
        login_to_login_view_pass.setVisibility(View.VISIBLE);
        login_but_login.setVisibility(View.GONE);
        login_to_forgot_pass_view.setVisibility(View.GONE);
        login_to_signup_view.setVisibility(View.GONE);
        login_but_reset_pass.setVisibility(View.GONE);
        terms_layout.setVisibility(View.GONE);
        login_name_layout.setVisibility(View.GONE);
        login_to_change_pass_view.setVisibility(View.GONE);
        change_password.setVisibility(View.VISIBLE);
        login_to_forgot_pass_view.setVisibility(View.GONE);


        login_layout_currentPassword.setVisibility(View.VISIBLE);
        login_layout_newPassword.setVisibility(View.VISIBLE);
        login_layout_email.setErrorEnabled(false);
        login_layout_email.setVisibility(View.VISIBLE);
        login_layout_phone.setVisibility(View.GONE);
        login_layout_phone.setErrorEnabled(false);
        login_layout_password.setVisibility(View.GONE);
        login_layout_password.setErrorEnabled(false);
        login_layout_password_repeat.setVisibility(View.GONE);
        login_layout_password_repeat.setErrorEnabled(false);
        login_layout_fname.setErrorEnabled(false);
        login_layout_lname.setErrorEnabled(false);
        login_layout_location.setVisibility(View.GONE);
        login_layout_location.setErrorEnabled(false);
    }

    private void networkLogin() {
        // TODO Create login logic here
        final String username = login_phone.getText().toString().trim();
        final String password= login_password.getText().toString().trim();

        if(username.equals("")) {
            login_phone.setError( "Required!" );
        }
        else if(password.equals(""))
        {
            login_password.setError( "Required!" );
        }
        else {
            userLogin();
        }

        //startActivity(new Intent(AppController.getInstance(), MainActivity.class));
    }

    private void toLoginView() {

        login_to_login_view.setVisibility(View.GONE);
        login_but_signup.setVisibility(View.GONE);
        login_to_login_view_pass.setVisibility(View.GONE);
        login_but_login.setVisibility(View.VISIBLE);
        login_to_forgot_pass_view.setVisibility(View.VISIBLE);
        login_to_signup_view.setVisibility(View.VISIBLE);
        login_but_reset_pass.setVisibility(View.GONE);
        terms_layout.setVisibility(View.GONE);
        login_name_layout.setVisibility(View.GONE);
        change_password.setVisibility(View.GONE);

        login_layout_currentPassword.setVisibility(View.GONE);
        login_layout_newPassword.setVisibility(View.GONE);
        login_layout_email.setErrorEnabled(false);
        login_layout_email.setVisibility(View.GONE);
        login_layout_phone.setVisibility(View.VISIBLE);
        login_layout_phone.setErrorEnabled(false);
        login_layout_password.setVisibility(View.VISIBLE);
        login_layout_password.setErrorEnabled(false);
        login_layout_password_repeat.setVisibility(View.GONE);
        login_layout_location.setVisibility(View.GONE);
        login_layout_location.setErrorEnabled(false);
        login_layout_password_repeat.setErrorEnabled(false);
        login_layout_fname.setErrorEnabled(false);
        login_layout_lname.setErrorEnabled(false);
    }

    private void toLoginResetPassword() {

        login_to_login_view.setVisibility(View.GONE);
        login_but_signup.setVisibility(View.GONE);
        login_to_login_view_pass.setVisibility(View.GONE);
        login_but_login.setVisibility(View.VISIBLE);
        login_to_forgot_pass_view.setVisibility(View.VISIBLE);
        login_to_signup_view.setVisibility(View.VISIBLE);
        login_but_reset_pass.setVisibility(View.GONE);
        terms_layout.setVisibility(View.GONE);
        login_name_layout.setVisibility(View.GONE);

        login_layout_currentPassword.setVisibility(View.GONE);
        login_layout_newPassword.setVisibility(View.GONE);
        login_layout_email.setErrorEnabled(false);
        login_layout_email.setVisibility(View.GONE);
        login_layout_phone.setVisibility(View.VISIBLE);
        login_layout_phone.setErrorEnabled(false);
        login_layout_password.setVisibility(View.VISIBLE);
        login_layout_password.setErrorEnabled(false);
        login_layout_password_repeat.setVisibility(View.GONE);
        login_layout_location.setVisibility(View.GONE);
        login_layout_location.setErrorEnabled(false);
        login_layout_password_repeat.setErrorEnabled(false);
        login_layout_fname.setErrorEnabled(false);
        login_layout_lname.setErrorEnabled(false);
//        login_to_login_view.setVisibility(View.GONE);
//        login_but_signup.setVisibility(View.GONE);
//        login_to_login_view_pass.setVisibility(View.VISIBLE);
//        login_but_login.setVisibility(View.GONE);
//        login_to_forgot_pass_view.setVisibility(View.GONE);
//        login_to_signup_view.setVisibility(View.GONE);
//        login_but_reset_pass.setVisibility(View.VISIBLE);
//        terms_layout.setVisibility(View.GONE);
//        login_name_layout.setVisibility(View.GONE);
//
//        login_layout_email.setErrorEnabled(false);
//        login_layout_email.setVisibility(View.GONE);
//        login_layout_phone.setVisibility(View.VISIBLE);
//        login_layout_phone.setErrorEnabled(false);
//        login_layout_password.setVisibility(View.VISIBLE);
//        login_layout_password.setErrorEnabled(false);
//        login_layout_password_repeat.setVisibility(View.GONE);
//        login_layout_password_repeat.setErrorEnabled(false);
//        login_layout_fname.setErrorEnabled(false);
//        login_layout_lname.setErrorEnabled(false);
//        login_layout_location.setVisibility(View.GONE);
//        login_layout_location.setErrorEnabled(false);

    }

//Auto generate password
    private void AutoGeneratePassword()
    {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++)
        {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String random_string = sb1.toString();
        Toast.makeText(LoginActivity.this, random_string,Toast.LENGTH_LONG).show();

                /**update password code goes here start**/
                //final String names = login_fname.getText().toString().trim();
                final String email = login_email.getText().toString().trim();
                final String phone = login_phone.getText().toString().trim();
                final String password =random_string;


                final ProgressDialog progress = ProgressDialog.show(this,"Loading", "Please wait...");

                StringRequest stringRequest = new StringRequest(Request.Method.POST, RESET_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                progress.dismiss();
                                Toast.makeText(LoginActivity.this, response, Toast.LENGTH_LONG).show();
                                JSONObject loginJson = null;
                                try {
                                    loginJson = new JSONObject(response);

                                    if (ParseUtil.contains(loginJson, "status"))
                                    {
                                        String status = loginJson.getString("status");
                                        String s=status;

                                        Toast.makeText(LoginActivity.this, response,Toast.LENGTH_LONG).show();

                                        if(s.trim().equals("200"))
                                        {
                                            Toast.makeText(LoginActivity.this, Html.fromHtml("<font color='#FF4500' ><b>" +"Check your Email Address For Password"+"</b></font>"),Toast.LENGTH_LONG).show();
                                            startActivity(new Intent(AppController.getInstance(), MainActivity.class));
                                        }
                                        else if(s.trim().equals("300"))
                                        {
                                            Toast.makeText(LoginActivity.this,Html.fromHtml("<font color='#FF4500' ><b>" +"Create New Password"+"</b></font>"),Toast.LENGTH_LONG).show();
                                        }
                                        else {
                                            Toast.makeText(LoginActivity.this,Html.fromHtml("<font color='#FF4500' ><b>" +"Unable to change"+"</b></font>"),Toast.LENGTH_LONG).show();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                                progress.dismiss();
                                     }
                        }) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        //params.put(KEY_LOGIN_FNAME, names);
                        params.put(KEY_EMAIL, email);
                        params.put(KEY_PHONE, phone);
                        params.put(KEY_PASSWORD, password);

                        //params.put(KEY_LOGIN_LNAME,loginlname);
                        //params.put(KEY_EMAIL,loginemail);

                        return params;
                    }

                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }

    public void ChangePassword()
    {
        /**update password code goes here start**/
        SharedPreferences editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String useid = editor.getString("KEY_USERID", "");

        final String id = useid;
        final String email = login_email.getText().toString().trim();
        final String passwords = login_password_currentPassword.getText().toString().trim();
        final String passwordnew = login_password_newPassword.getText().toString().trim();


        final ProgressDialog progress = ProgressDialog.show(this,"Loading", "Please wait...");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, CHANGE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        Toast.makeText(LoginActivity.this, response, Toast.LENGTH_LONG).show();
                        JSONObject loginJson = null;
                        try {
                            loginJson = new JSONObject(response);

                            if (ParseUtil.contains(loginJson, "status"))
                            {
                                String status = loginJson.getString("status");
                                String s=status;

                                Toast.makeText(LoginActivity.this, response,Toast.LENGTH_LONG).show();

                                if(s.trim().equals("200"))
                                {
                                    Toast.makeText(LoginActivity.this, Html.fromHtml("<font color='#FF4500' ><b>" +"Check your Email Address For Password"+"</b></font>"),Toast.LENGTH_LONG).show();
                                    startActivity(new Intent(AppController.getInstance(), MainActivity.class));
                                }
                                else if(s.trim().equals("300"))
                                {
                                    Toast.makeText(LoginActivity.this,Html.fromHtml("<font color='#FF4500' ><b>" +"Please Try Again"+"</b></font>"),Toast.LENGTH_LONG).show();
                                }
                                else {
                                    Toast.makeText(LoginActivity.this,Html.fromHtml("<font color='#FF4500' ><b>" +"Unable to change. Please Try Again"+"</b></font>"),Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        progress.dismiss();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_EMAIL, email);
                params.put(KEY_CURENTPASS, passwords);
                params.put(KEY_NEWPASS, passwordnew);
                params.put(KEY_ID, id);

/******************************************************************************************************************/

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void signup()

    {
        if (check_terms_conditions.isChecked() ) {

            /**signup code goes here start**/
            final String names = login_fname.getText().toString().trim();
            final String location = login_location.getText().toString().trim();
            final String phone = login_phone.getText().toString().trim();
            final String password = login_password.getText().toString().trim();

//        final String loginlname=login_lname.getText().toString().trim();
//        final String loginemail= login_email.getText().toString().trim();
            final ProgressDialog progress = ProgressDialog.show(this,"Loading", "Please wait...");

            StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progress.dismiss();
                           //Toast.makeText(LoginActivity.this, response, Toast.LENGTH_LONG).show();
                            JSONObject loginJson = null;
                            try {
                                loginJson = new JSONObject(response);

                                if (ParseUtil.contains(loginJson, "status"))
                                {
                                    String status = loginJson.getString("status");
                                    String s=status;
//                                    if(status=="404")
//                                    {
//                                        Toast.makeText(LoginActivity.this,"Unable to SignUp try again",Toast.LENGTH_LONG).show();
//                                    }
//
                                    if(s.trim().equals("200"))
                                    {
                                        Toast.makeText(LoginActivity.this, Html.fromHtml("<font color='#FF4500' ><b>" +"successfully SignedUp"+"</b></font>"),Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(AppController.getInstance(), MainActivity.class));
                                    }
                                    else if(s.trim().equals("300"))
                                    {
                                        Toast.makeText(LoginActivity.this,Html.fromHtml("<font color='#FF4500' ><b>" +"phone number exist"+"</b></font>"),Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        Toast.makeText(LoginActivity.this,Html.fromHtml("<font color='#FF4500' ><b>" +"Unable to SignUp try again"+"</b></font>"),Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            //Toast.makeText(LoginActivity.this,"failed to create account",Toast.LENGTH_LONG).show();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(KEY_LOCATION, location);
                    params.put(KEY_PHONE, phone);
                    params.put(KEY_PASSWORD, password);

                    //params.put(KEY_LOGIN_LNAME,loginlname);
                    //params.put(KEY_EMAIL,loginemail);

                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else {
            Toast.makeText(LoginActivity.this, "agree our terms and conditions", Toast.LENGTH_LONG).show();
        }
    }

    private void userLogin() {

        final ProgressDialog progress = ProgressDialog.show(this,"Loading", "Please wait...");

//        loginemail= login_email.getText().toString().trim();
//        loginpassword_username = login_password.getText().toString().trim();

       // sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final String username = login_phone.getText().toString().trim();
        final String password= login_password.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
                   // int status;

                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                        JSONObject loginJson = null;
                        try {
                            loginJson = new JSONObject(response);
                            //status=response.get
                            if(ParseUtil.contains(loginJson, "status"))
                            {
                                int status = loginJson.getInt("status");
                                String id= loginJson.getString("id");
                                String names=loginJson.getString("names");

                                if(status==200)
                                {
                                    SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
                                    editor.putString(KEY_LOGIN_PHONE_NUMBER,username);
                                    editor.putString(KEY_LOGIN_PASSWORD,password);
                                    editor.putString("KEY_USERID",id);
                                    editor.putString("KEY_USERNAME",names);
                                    editor.commit();

//                                    Toast.makeText(LoginActivity.this, id,Toast.LENGTH_LONG).show();
//                                    Toast.makeText(LoginActivity.this, names,Toast.LENGTH_LONG).show();
                                    SharedPreferences fragids = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    int Ratefragment = fragids.getInt("RATEID", 0);
                                    int pendingFragment = fragids.getInt("PENDING", 0);
                                    int emergencyFragment = fragids.getInt("EMERGENCYID", 0);
                                    int manageFragment = fragids.getInt("MANAGE", 0);
                                    String phone = fragids.getString(KEY_LOGIN_PHONE_NUMBER,null);
                                    String pssword = fragids.getString(KEY_LOGIN_PASSWORD,null);

                                    if(Ratefragment==1)
                                    {
//                                        getActivity().finish();
                                        startActivity(new Intent(AppController.getInstance(), MainActivity.class));

//                                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                                        ft.replace(R.id.content_frame, new Rating(), getString(R.string.fragment_tag_home));
//                                        ft.commit();
                                    }
                                    else if(pendingFragment==2)
                                    {
                                        startActivity(new Intent(AppController.getInstance(), MainActivity.class));

//                                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                                        ft.replace(R.id.content_frame, new pending_jobs(), getString(R.string.fragment_tag_home));
//                                        ft.commit();
                                    }
                                    else if(emergencyFragment==3)
                                    {
                                        startActivity(new Intent(AppController.getInstance(), MainActivity.class));

//                                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                                        ft.replace(R.id.content_frame, new EmergencyBook(), getString(R.string.fragment_tag_home));
//                                        ft.commit();
                                    }
                                    else if(manageFragment==4)
                                    {
                                    startActivity(new Intent(AppController.getInstance(), MainActivity.class));
                                    }
                                    else {
                                        startActivity(new Intent(AppController.getInstance(), Booking.class));
                                    }
                                }
                                else{
                                    Toast.makeText(LoginActivity.this, "wrong",Toast.LENGTH_LONG).show();
                                }

                            }
//                            else {
//                                Toast.makeText(LoginActivity.this, "Username and Password don't match...", Toast.LENGTH_LONG).show();
//                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast toast=Toast.makeText(LoginActivity.this, Html.fromHtml("<font color='#FF4500' ><b>" +"Username and Password don't match..."+"</b></font>"), Toast.LENGTH_LONG);
                            //toast.setGravity(Gravity.TOP, 50, 0);
                            toast.show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                       // Toast.makeText(LoginActivity.this,error.toString(),Toast.LENGTH_LONG ).show();
                       // Toast.makeText(LoginActivity.this, "Username and Password don't match...", Toast.LENGTH_LONG).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();

                map.put(KEY_LOGIN_PHONE_NUMBER,username);
                map.put(KEY_LOGIN_PASSWORD,password);

                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //retrive the share fragment ids
    public void GetFramentIds()
    {
        SharedPreferences editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String Ratefragment = editor.getString("RATEID", "");
        String usename = editor.getString("KEY_USERNAME", "");
        String phone = editor.getString(KEY_LOGIN_PHONE_NUMBER,null);
        String pssword = editor.getString(KEY_LOGIN_PASSWORD,null);

    }

}
