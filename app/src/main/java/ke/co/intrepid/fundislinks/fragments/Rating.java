package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import ke.co.intrepid.fundislinks.activities.LoginActivity;
import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.adapter.RateAdapter;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

public class Rating extends Fragment {
    private ShareActionProvider mShareActionProvider;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String KEY_USERID="useid";
    public static final String KEY_USERNAME="usename";
    public static final String KEY_LOGIN_PHONE_NUMBER="username";
    public static final String KEY_LOGIN_PASSWORD="password";

    String  rateid,ratedescription,ratedate_booked,ratenames,datecreated,timeCreated,rating;
    private String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Image> images;
    private ProgressDialog rateDialog;
    private RateAdapter mAdapter;
    private RecyclerView recyclerView;
    public static final String rate_url="http://dashboard.fundislink.com/Json/Json/complete_jobs_unrated/";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("Rate");

        return inflater.inflate(R.layout.fragment_rating, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_rating);
        recyclerView.setHasFixedSize(true);

        rateDialog = new ProgressDialog(getActivity());
        images = new ArrayList<>();
        mAdapter = new RateAdapter(getActivity(), images);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RateAdapter.RecyclerTouchListener(getActivity(), recyclerView, new RateAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                Image image = images.get(position);
                rateid =image.getRateid();
                ratedescription=image.getRatedescription();
                ratedate_booked=image.getRatedate_booked();
                ratenames=image.getRatenames();
                datecreated=image.getDatecreated();
                timeCreated=image.getTimeCreated();
                rating=image.getRatefundi_id();

                //Put the value

                rate_me ldf = new rate_me ();
                Bundle args = new Bundle();
                args.putString("KEY_FUNDIID", rateid);
                args.putString("KEY_ratedescription", ratedescription);
                args.putString("KEY_ratedate_booked", ratedate_booked);
                args.putString("KEY_ratenames", ratenames);
                args.putString("KEY_datecreated", datecreated);
                args.putString("KEY_timeCreated", timeCreated);
                args.putString("KEY_rating", rating);

                ldf.setArguments(args);
                //Inflate the fragment
                getFragmentManager().beginTransaction().add(R.id.content_frame, ldf).commit();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        fetchImages();


    }

    private void fetchImages() {
        SharedPreferences editor = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String useid = editor.getString("KEY_USERID", "");
        String usename = editor.getString("KEY_USERNAME", "");
        String phone = editor.getString(KEY_LOGIN_PHONE_NUMBER,null);
        String pssword = editor.getString(KEY_LOGIN_PASSWORD,null);

        if (useid == "") {
            final ProgressDialog loading = ProgressDialog.show(getActivity(),"LogIN To", "Rate");
            loading.dismiss();
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        }
        else{
             final ProgressDialog pDialog = ProgressDialog.show(getActivity(),"Loading Rating","please wait...");
            pDialog.setCancelable(true);

            String user_id=(useid);
        JsonArrayRequest req = new JsonArrayRequest(rate_url+user_id,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.dismiss();

                        images.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Image image = new Image();

                                image.setRateid(object.getString("id"));
                                image.setRatedescription("Description: "+object.getString("description"));
                                image.setRatedate_booked(object.getString("date_booked"));
                                image.setRatefundi_id(object.getString("fundi_id"));
                                image.setRatenames(object.getString("names"));
                                image.setDatecreated(object.getString("date_created"));
                                image.setTimeCreated(object.getString("time_created"));

                                images.add(image);

                            } catch (JSONException e) {
                                Log.e(TAG, "Json parsing error: " + e.getMessage());
                            }
                            if(response==null)
                            {

                            }
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                rateDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        }

    }

}
