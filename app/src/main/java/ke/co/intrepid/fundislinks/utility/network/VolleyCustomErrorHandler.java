package ke.co.intrepid.fundislinks.utility.network;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import ke.co.intrepid.fundislinks.application.MyApplication;
import ke.co.intrepid.fundislinks.log.L;

/**
 * Created by Clifford Owino on 9/14/2016.
 */
public class VolleyCustomErrorHandler {

    public static void errorMessage(VolleyError error){

        if(error instanceof TimeoutError){

            L.T(MyApplication.getAppContext(), "Your request has been timed out, slow internet connection maybe. Just try again");
            L.m("Request Timeout Error");

        } else if(error instanceof NoConnectionError){

            L.t(MyApplication.getAppContext(), "No Connection currently, check your data plan and retry");
            L.m("No Connection Error");


        }else if(error instanceof NetworkError){

            L.t(MyApplication.getAppContext(), "Network Error, just try again please");
            L.m("Network Error");

        }else if(error instanceof ServerError){

            L.t(MyApplication.getAppContext(), "Error from our servers, don't worry just retry after a few minutes");
            L.m("Server Error");

        }else if(error instanceof ParseError){

            L.t(MyApplication.getAppContext(), "Parsing error, don't worry just retry after a few minutes");
            L.m("Data Parse Error");

        }

    }
}
