package ke.co.intrepid.fundislinks.app;

/**
 * Created by Joshua test on 9/11/2016.
 */

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;


public class AppController extends android.support.multidex.MultiDexApplication {
    private ImageLoader mImageloader;
    private ke.co.intrepid.fundislinks.app.LruBitmapCache mLruBitmapCache;
    public static final String TAG = AppController.class
            .getSimpleName();

    private RequestQueue mRequestQueue;

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageloader == null) {
            getLruBitmapCache();
            mImageloader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
        }
        return this.mImageloader;
    }

    private ke.co.intrepid.fundislinks.app.LruBitmapCache getLruBitmapCache() {
        if (mLruBitmapCache == null)
            mLruBitmapCache = new ke.co.intrepid.fundislinks.app.LruBitmapCache();
            return this.mLruBitmapCache;
        }

}