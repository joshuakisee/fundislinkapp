package ke.co.intrepid.fundislinks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.utility.ParseUtil;
import ke.co.intrepid.fundislinks.R;


public class rate_me extends Fragment {
    TextView rateId, ratename, datebooked, datecreated, timecreated, description, fundi_ids;
    RatingBar ratebar;

    private static final String subrate_url="http://dashboard.fundislink.com/Json/Json/create_rating";

    public static final String KEY_JOB_ID="job_id";
    public static final String KEY_JOB_RATING="rating";
    public static final String KEY_FUNDIS_ID="fundi_id";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("Rate");

        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_rate_me, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rateId = (TextView) view.findViewById(R.id.fundis_id_rating);
        ratename = (TextView) view.findViewById(R.id.name_fundi_rating);
        datebooked = (TextView) view.findViewById(R.id.date_booked_description);
        datecreated = (TextView) view.findViewById(R.id.datecreated);
        timecreated = (TextView) view.findViewById(R.id.time_created);
        description = (TextView) view.findViewById(R.id.rateDescription);
        fundi_ids = (TextView) view.findViewById(R.id.ratefundiID);
        ratebar=(RatingBar) view.findViewById(R.id.ratingBar);

        String value = getArguments().getString("KEY_FUNDIID");
        String ratenamess = getArguments().getString("KEY_ratenames");
        String Ratedate_bookedd = getArguments().getString("KEY_ratedate_booked");
        String Datecreatedd = getArguments().getString("KEY_datecreated");
        String TimeCreatedd = getArguments().getString("KEY_timeCreated");
        String Ratedescriptionn = getArguments().getString("KEY_ratedescription");
        String Rateid = getArguments().getString("KEY_rating");

        rateId.setText(value);
        ratename.setText(ratenamess);
        datebooked.setText(Ratedate_bookedd);
        datecreated.setText(Datecreatedd);
        timecreated.setText(TimeCreatedd);
        description.setText(Ratedescriptionn);
        fundi_ids.setText(Rateid);

        //Toast.makeText(getActivity(), value, Toast.LENGTH_SHORT).show();

        //rateAssign();

        Button buttonrate=(Button) view.findViewById(R.id.buttonrate);
        buttonrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                book_job();
            }
        });
    }

    /**start saving rating**/
    private void book_job()
    {
        //String.valueOf(rate+1);


        final String job_id=rateId.getText().toString().trim();
        final String rating=String.valueOf(ratebar.getRating());
        final String fundi_id=fundi_ids.getText().toString().trim();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, subrate_url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        JSONObject loginJson = null;
                        try {
                            loginJson = new JSONObject(response);
                            //status=response.get
                            if (ParseUtil.contains(loginJson, "status"))
                            {
                                int status = loginJson.getInt("status");


                                if(status==200)
                                {
                                    Toast.makeText(getActivity(), "fundi rated", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(getActivity(), "error Try again",Toast.LENGTH_LONG).show();
                                        }

                            }
                            else {
                                Toast.makeText(getActivity(), "error Try again",Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getActivity(),"error while Booking",Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                    params.put(KEY_JOB_ID,job_id);
                    params.put(KEY_JOB_RATING,rating);
                    params.put(KEY_FUNDIS_ID,fundi_id);

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }
    /**end saving rating**/
}
