package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ke.co.intrepid.fundislinks.adapter.CardAdapter;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

/**
 * Created by Clifford Owino on 9/14/2016.
 */

public class HomeFragment extends Fragment {
    private List<Image> listimages;
    //Creating Views
    NetworkImageView mVolleyImageView;
    TextView mytitle;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    public static final String db_url="http://dashboard.fundislink.com/Json/Json/speciality_category";
    public static final String image_url = "http://dashboard.fundislink.com/image_uploads/category_images/";

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_homeweb, container, false);
        //Initializing Views
        mVolleyImageView = (NetworkImageView) view.findViewById(R.id.thumbnail);
        mytitle=(TextView) view.findViewById(R.id.mytitle);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        //layoutManager = new LinearLayoutManager(getActivity());
        layoutManager=new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(layoutManager);

        //Initializing our listimages list
        listimages = new ArrayList<>();


        isOnline();

        return view;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null)
        {
           // Calling method to get data
            getData();
            /**get only images**/
//            ImageRequest imageRequest = new ImageRequest(image_url, new Response.Listener<Bitmap>() {
//                @Override
//                public void onResponse(Bitmap response) {
//
//                    mVolleyImageView.setImageBitmap(response);
//                }
//            },
//                    0,
//                    0,
//                    ImageView.ScaleType.CENTER_CROP,
//                    null,
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // show an error drawable
//                            //mVolleyImageView.setImageDrawable(getDrawable(R.drawable.error));
//                        }
//                    }){
//                @Override
//                public Request.Priority getPriority(){
//                    return Request.Priority.LOW;
//                }
//            };
//            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//            requestQueue.add(imageRequest);
            /**end of getting images**/
        }
        else {
            fetchFromVolleyCache();
            final ProgressDialog loading = ProgressDialog.show(getActivity(),"Network Error", "Try again",true);
            Runnable progressRunnable = new Runnable() {

                @Override
                public void run() {
                    loading.cancel();
                }
            };

            Handler pdCanceller = new Handler();
            pdCanceller.postDelayed(progressRunnable, 3000);
        }

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();

    }
    //This method will get data from the web api
    private void getData(){
        //Showing a progress dialog
        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Loading Fundis", "Please wait...");

        //Creating a json array request
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(db_url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //Dismissing progress dialog
                        loading.dismiss();

                        //calling method to parse json array
                        parseData(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //Creating request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(jsonArrayRequest);
    }



    //This method will parse json data
    private void parseData(JSONArray array){
        for(int i = 0; i<array.length(); i++) {
            Image image = new Image();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);

                image.setTitle(json.getString("title"));
                image.setId(json.getString("id"));
                image.setImage_url(image_url+json.getString("icon"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            listimages.add(image);
        }

        //Finally initializing our adapter
        adapter = new CardAdapter(listimages, getActivity());

        //Adding adapter to recyclerview
        recyclerView.setAdapter(adapter);
    }

    private boolean fetchFromVolleyCache(){
        try{
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            Cache cache = requestQueue.getCache();
            Cache.Entry entry = cache.get(db_url);
            if(entry != null) {
                JSONObject cachedResponse = new JSONObject(new String(entry.data, "UTF-8"));
                mytitle.setText(cachedResponse.getJSONObject("value").getString("title"));
                Toast.makeText(getActivity(), "fetched from cache!", Toast.LENGTH_SHORT).show();
                return true;
            }
        }catch (UnsupportedEncodingException | JSONException e){
            e.printStackTrace();
           // Log.d(TAG, "Error fetching joke from cache for entry: " + RANDOM_URL);
            Toast.makeText(getActivity(), "Error fetching joke from cache for entry: ", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    /**CACHE*88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888**/

    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data);
            JSONObject jsonObject = new JSONObject(jsonString);

            // force response to be cached
            Map<String, String> headers = response.headers;
            long cacheExpiration = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            Cache.Entry entry = new Cache.Entry();
            entry.data = response.data;
            entry.etag = headers.get("ETag");
            entry.ttl = now + cacheExpiration;
            entry.serverDate = HttpHeaderParser.parseDateAsEpoch(headers.get("Date"));
            entry.responseHeaders = headers;

            return Response.success(jsonObject, entry);
        } catch (JSONException e) {
            e.printStackTrace();
            return Response.error(new VolleyError("Error parsing network response"));
        }
    }
    /**88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888**/

}
