package ke.co.intrepid.fundislinks.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ke.co.intrepid.fundislinks.pojo.Keys;
import ke.co.intrepid.fundislinks.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               startActivity(new Intent(SplashActivity.this, MainActivity.class));
                //startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();

            }
        }, Keys.keys.SPLASH_DISPLAY_TIMER);
    }
}