package ke.co.intrepid.fundislinks.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

/**
 * Created by joshua on 11/17/2016.
 */
public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.MyViewHolder> {

    private List<Image> images;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        //public ImageView thumbnail;
        TextView jobid, jobdate, jobdescription;

        public MyViewHolder(View view) {
            super(view);
            //thumbnail = (ImageView) view.findViewById(R.id.available_agent_pic);
            // agentdescription = (TextView) view.findViewById(R.id.Agent_description);
            jobid = (TextView) view.findViewById(R.id.jobid);
            jobdate = (TextView) view.findViewById(R.id.jobdate);
            jobdescription = (TextView) view.findViewById(R.id.pendingjobdescription);
        }
    }


    public PendingAdapter(Context context, List<Image> images) {
        mContext = context;
        this.images = images;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pending_jobs_content, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Image image = images.get(position);
//        Glide.with(mContext).load(image.getAgentimage())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.thumbnail);

        holder.jobid.setText(image.getPendingid());
        holder.jobdate.setText(image.getPendingdatecreated());
        holder.jobdescription.setText(image.getPendingdescription());

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private PendingAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final PendingAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
