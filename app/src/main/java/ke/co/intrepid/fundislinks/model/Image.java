package ke.co.intrepid.fundislinks.model;

/**
 * Created by Joshua_test on 9/11/2016.
 * this class contains only the fundis
 */

import java.io.Serializable;

public class Image implements Serializable {
    /**starting the fundis getters and setters**/
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    /**ending the fundis getters and setters**/

    private   String title;//for the fundis
    private   String image_url;//for the fundis
    private   String id;//for the fundis

    /**starting the services getters and setters**/
    public String getId_services() {
        return id_services;}

    public void setId_services(String id_services) {
        this.id_services = id_services;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle_services() {
        return title_services;
    }

    public void setTitle_services(String title_services) {
        this.title_services = title_services;
    }

    private String id_services;//for the services
    private String description;//for the services
    private String title_services;//for the services
    /***ending the services getters and setters***/


    /**Agents getters and setters**/
    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getAgentbussinessname() {
        return agentbussinessname;
    }

    public void setAgentbussinessname(String agentbussinessname) {
        this.agentbussinessname = agentbussinessname;
    }

    public String getAgentdescription() {
        return agentdescription;
    }

    public void setAgentdescription(String agentdescription) {
        this.agentdescription = agentdescription;
    }

    public String getAgentlocation() {
        return agentlocation;
    }

    public void setAgentlocation(String agentlocation) {
        this.agentlocation = agentlocation;
    }


//    public void setAgentimage(String agentimage) {
//        this.agentimage = agentimage;
//    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }

    private String agentname;
    private String agentid;
    private String agentbussinessname;
    private String agentlocation;
    private String agentdescription;

    public String getAgentimage() {
        return agentimage;
    }

    public void setAgentimage(String agentimage) {
        this.agentimage = agentimage;
    }

    public String agentimage;
    /**end of Agents getters and setters**/


    /**start available agent getter $$ setters**/
    public String getAvailableagentname() {
        return availableagentname;
    }

    public void setAvailableagentname(String availableagentname) {
        this.availableagentname = availableagentname;
    }

    public String getAvailbeagentid() {
        return availbeagentid;
    }

    public void setAvailbeagentid(String availbeagentid) {
        this.availbeagentid = availbeagentid;
    }

    public String getAvailableagentbussinessname() {
        return availableagentbussinessname;
    }

    public void setAvailableagentbussinessname(String availableagentbussinessname) {
        this.availableagentbussinessname = availableagentbussinessname;
    }

    public String getAvailableagentlocation() {
        return availableagentlocation;
    }

    public void setAvailableagentlocation(String availableagentlocation) {
        this.availableagentlocation = availableagentlocation;
    }

    public String getAvailableagentdescription() {
        return availableagentdescription;
    }

    public void setAvailableagentdescription(String availableagentdescription) {
        this.availableagentdescription = availableagentdescription;
    }

    public String getAvailableagentimage() {
        return availableagentimage;
    }

    public void setAvailableagentimage(String availableagentimage) {
        this.availableagentimage = availableagentimage;
    }

    private String availableagentname;
    private String availbeagentid;
    private String availableagentbussinessname;
    private String availableagentlocation;
    private String availableagentdescription;
    public String availableagentimage;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String phone;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String rating;
    /**end available agent getter $$ setters**/

    /**start terms and conditions/ conduct us**/
    public String getConducttitle() {
        return conducttitle;
    }

    public void setConducttitle(String conducttitle) {
        this.conducttitle = conducttitle;
    }

    public String getConductdescription() {
        return conductdescription;
    }

    public void setConductdescription(String conductdescription) {
        this.conductdescription = conductdescription;}

    private String conducttitle;
    private String conductdescription;
//    private String AccountManager;
//    private String AccountManager1;

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    private String terms;

    public String getTerms_description() {
        return terms_description;
    }

    public void setTerms_description(String terms_description) {
        this.terms_description = terms_description;
    }

    private String terms_description;
    /**end terms and conditions/ conduct us**/

    /**start rating getters and setters**/
    public String getRatedescription() {
        return ratedescription;
    }

    public void setRatedescription(String ratedescription) {
        this.ratedescription = ratedescription;
    }

    public String getRateid() {
        return rateid;
    }

    public void setRateid(String rateid) {
        this.rateid = rateid;
    }

    public String getRatedate_booked() {
        return ratedate_booked;
    }

    public void setRatedate_booked(String ratedate_booked) {
        this.ratedate_booked = ratedate_booked;
    }

    public String getRatenames() {
        return ratenames;
    }

    public void setRatenames(String ratenames) {
        this.ratenames = ratenames;
    }

    public String getRatefundi_id() {
        return ratefundi_id;
    }

    public void setRatefundi_id(String ratefundi_id) {
        this.ratefundi_id = ratefundi_id;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String rateid;
    public String ratedescription;
    public String ratedate_booked;
    public String ratenames;
    public String ratefundi_id;
    public String datecreated;
    public String timeCreated;
    /**end rating getters and setters**/


    /**start emergency setter and getters**/
    public String getMycontent() {
        return mycontent;
    }

    public void setMycontent(String mycontent) {
        this.mycontent = mycontent;
    }
    public String getSubs() {
        return subs;
    }

    public void setSubs(String subs) {
        this.subs = subs;
    }

    public String getMycategoryId() {
        return mycategoryId;
    }

    public void setMycategoryId(String mycategoryId) {
        this.mycategoryId = mycategoryId;
    }


    public String mycontent;
    public String subs;
    public String mycategoryId;
    /**emergency sub category setter and getters**/
    public String getSubcategoriesid() {
        return subcategoriesid;
    }

    public void setSubcategoriesid(String subcategoriesid) {
        this.subcategoriesid = subcategoriesid;
    }

    public String getSub_categories() {
        return sub_categories;
    }

    public void setSub_categories(String sub_categories) {
        this.sub_categories = sub_categories;
    }

    public String subcategoriesid;
    public String sub_categories;
    /**end emergency setter and getters**/



    /**start pending jobd***/
    public String getPendingid() {
        return pendingid;
    }

    public void setPendingid(String pendingid) {
        this.pendingid = pendingid;
    }

    public String getPendingdescription() {
        return pendingdescription;
    }

    public void setPendingdescription(String pendingdescription) {
        this.pendingdescription = pendingdescription;
    }

    public String getPendingdatecreated() {
        return pendingdatecreated;
    }

    public void setPendingdatecreated(String pendingdatecreated) {
        this.pendingdatecreated = pendingdatecreated;
    }


    public String pendingid;
    public String pendingdescription;
    public String pendingdatecreated;
    /**end pending jobd***/
    public Image() {
    }
}

