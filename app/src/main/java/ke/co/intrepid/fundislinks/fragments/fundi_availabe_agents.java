package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ke.co.intrepid.fundislinks.activities.MainActivity;

import ke.co.intrepid.fundislinks.adapter.AvailableAgentAdapter;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

public class fundi_availabe_agents extends Fragment {
    public static String agent,name,location,description,agentimage,agentName;
    private String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Image> images;
    private ProgressDialog pDialog;
    private AvailableAgentAdapter mAdapter;
    private RecyclerView recyclerView;
    public static final String agent_url="http://dashboard.fundislink.com/Json/Json/agents/";
    public static final String agent_image_url= "http://dashboard.fundislink.com/image_uploads/agents/";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("FundisLink Fundis");

        return inflater.inflate(R.layout.fragment_fundi_availabe_agents, container, false);



    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_available_agents);
        recyclerView.setHasFixedSize(true);

        pDialog = new ProgressDialog(getActivity());
        images = new ArrayList<>();
        mAdapter = new AvailableAgentAdapter(getActivity(), images);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new AvailableAgentAdapter.RecyclerTouchListener(getActivity(), recyclerView, new AvailableAgentAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

               Image image = images.get(position);
                agentimage=image.getAgentimage();
                agent =image.getAgentid();
                name=image.getAgentbussinessname();
                location=image.getAvailableagentlocation();
                description=image.getAgentdescription();
                agentName=image.getAgentname();

             FragmentManager fm=getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new agentprofile()).addToBackStack(null).commit();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        isOnline();


    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null)
        {
            // Calling method to get data
            fetchImages();
        }
        else {
            final ProgressDialog loading = ProgressDialog.show(getActivity(),"Network Error", "Try again",true);
            Runnable progressRunnable = new Runnable() {

                @Override
                public void run() {
                    loading.cancel();
                    FragmentManager fm=getFragmentManager();
                    fm.beginTransaction().replace(R.id.content_frame, new FundiProcess()).commit();
                }
            };

            Handler pdCanceller = new Handler();
            pdCanceller.postDelayed(progressRunnable, 3000);


        }

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();

    }

    private void fetchImages() {

        Fundi_services s=new Fundi_services();
        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Loading Fundi...", "Agents",true);


        JsonArrayRequest req = new JsonArrayRequest(agent_url+s.service,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        loading.hide();

                        images.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Image image = new Image();

                                image.setAgentid(object.getString("id"));
                                image.setAgentbussinessname(object.getString("business_name"));
                                image.setAvailableagentlocation(object.getString("location"));
                                image.setAgentdescription(object.getString("description"));
                                image.setAgentimage(agent_image_url+object.getString("agent_images"));
                                image.setAgentname(object.getString("names"));

                                images.add(image);

                            } catch (JSONException e) {
                                Log.e(TAG, "Json parsing error: " + e.getMessage());
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }
}
