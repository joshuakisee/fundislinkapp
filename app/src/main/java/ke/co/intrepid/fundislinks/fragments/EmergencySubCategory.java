package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ke.co.intrepid.fundislinks.R;
import ke.co.intrepid.fundislinks.activities.LoginActivity;
import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.adapter.EmergencySubAdapter;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.log.L;
import ke.co.intrepid.fundislinks.model.Image;

public class EmergencySubCategory extends Fragment {
    public static final String MyPREFERENCES = "MyPrefs" ;
    String  subid,subcategory;
    String idss,categoryname;
    private ProgressDialog pDialog;
    private EmergencySubAdapter mAdapter;
    private ArrayList<Image> images;
    private String TAG = MainActivity.class.getSimpleName();
    public static final String jobs_url="http://dashboard.fundislink.com/Json/Json/emergency_subservices/";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public EmergencySubCategory() {
        // Required empty public constructor
    }
    
    // TODO: Rename and change types and number of parameters
    public static EmergencySubCategory newInstance(String param1, String param2) {
        EmergencySubCategory fragment = new EmergencySubCategory();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Retrieve the value
        String value = getArguments().getString("category");

        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle(value);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_emergency_sub_category, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.subcategory);
        recyclerView.setHasFixedSize(true);

        pDialog = new ProgressDialog(getActivity());
        images = new ArrayList<>();
        mAdapter = new EmergencySubAdapter(getActivity(), images);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        recyclerView.addOnItemTouchListener(new EmergencySubAdapter.RecyclerTouchListener(getActivity(), recyclerView, new EmergencySubAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                Image image = images.get(position);
                subid=image.getSubcategoriesid();
                subcategory=image.getSub_categories();
//                Toast.makeText(getActivity(), jobs, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getActivity(), subid, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getActivity(), subcategory, Toast.LENGTH_SHORT).show();
                //share the rate id
                SharedPreferences.Editor subcatid = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
                subcatid.putString("SUBCATEGORY",subid);
                subcatid.commit();
                //end of sharing.

                SharedPreferences editor = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                String useid = editor.getString("KEY_USERID", "");

                if (useid == "") {
                    final ProgressDialog loading = ProgressDialog.show(getActivity(),"LogIN To view", "Pending");
                    loading.dismiss();
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                }
                else{
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_frame, new EmergencyBook(), getString(R.string.fragment_tag_home));
                    ft.addToBackStack(null).commit();
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        fetchImages();


    }
    private void fetchImages() {
        //Retrieve the value
        String value = getArguments().getString("categoryid");

        pDialog.setMessage("Loading Emergency jobs...");
        pDialog.show();


        JsonArrayRequest req = new JsonArrayRequest(jobs_url+value,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();

                        images.clear();

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Image image = new Image();


                                JSONObject subs=object.getJSONObject("subs");

                                image.setSubcategoriesid(subs.getString("sub_categories_id"));
                                image.setSub_categories(subs.getString("sub_categories"));

                                images.add(image);

                            } catch (JSONException e) {
                                Log.e(TAG, "Json parsing error: " + e.getMessage());
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
