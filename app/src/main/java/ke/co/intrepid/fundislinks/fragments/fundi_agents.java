package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ke.co.intrepid.fundislinks.activities.MainActivity;

import ke.co.intrepid.fundislinks.adapter.AgentAdapter;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

public class fundi_agents extends Fragment {
    public static String fundiId;
    public static String availableName;
    public static String availableLocation;
    public static String rating;
    public static String availabledescription;
    public static String pic,phone;

    private String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Image> images;
    private ProgressDialog pDialog;
    private AgentAdapter mAdapter;
    private RecyclerView recyclerView;
    public static final String agent_url="http://dashboard.fundislink.com/Json/Json/fundis/";
    public static final String agent_image_url="http://dashboard.fundislink.com/image_uploads/fundis_images/";
    public static String description="Description:";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("FundisLink Agents");

        return inflater.inflate(R.layout.fragment_fundi_agents, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SpannableString reddescription= new SpannableString(description);
        reddescription.setSpan(new ForegroundColorSpan(Color.RED), 0, description.length(), 0);
        //builder.append(redSpannable);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_agents);
        recyclerView.setHasFixedSize(true);

        pDialog = new ProgressDialog(getActivity());
        images = new ArrayList<>();
        mAdapter = new AgentAdapter(getActivity(), images);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new AgentAdapter.RecyclerTouchListener(getActivity(), recyclerView, new AgentAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                Image image = images.get(position);

                pic=image.getAgentimage();
                fundiId =image.getAgentid();
                availableName=image.getAgentname();
                availableLocation=image.getAgentlocation();
                rating=image.getRating();
                availabledescription=image.getAgentdescription();
                phone=image.getPhone();


                /**share image and sent to nxt fragment**/
                SharedPreferences sharepic = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor edit=sharepic.edit();
                edit.putString("image_data",pic);
                edit.putString("phone",phone);
                edit.commit();

                FragmentManager fm=getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new AvailableAgentProfilee()).addToBackStack(null).commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        isOnline();


    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null)
        {
            // Calling method to get data
            fetchImages();
        }
        else {
            final ProgressDialog loading = ProgressDialog.show(getActivity(),"Network Error", "Try again",true);
            Runnable progressRunnable = new Runnable() {

                @Override
                public void run() {
                    loading.cancel();
                    FragmentManager fm=getFragmentManager();
                    fm.beginTransaction().replace(R.id.content_frame, new FundiProcess()).commit();
                }
            };

            Handler pdCanceller = new Handler();
            pdCanceller.postDelayed(progressRunnable, 3000);


        }

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();

    }
    private void fetchImages() {

        Fundi_services s=new Fundi_services();
        fundi_availabe_agents avaibleagentid=new fundi_availabe_agents();

        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Loading Fundi...", "agents",true);
        JsonArrayRequest req = new JsonArrayRequest(agent_url+avaibleagentid.agent+"/"+s.service,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        loading.hide();

                        images.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Image image = new Image();

                                image.setAgentid(object.getString("id"));

                                image.setAgentname(object.getString("fundi_name"));
                                image.setAgentlocation(object.getString("residence"));
                                image.setAgentdescription(description+object.getString("description"));
                                image.setAgentimage(agent_image_url+object.getString("agent_images"));
                                image.setRating(object.getString("rating"));
                                image.setPhone(object.getString("phone"));

                                images.add(image);

                            } catch (JSONException e) {
                                Log.e(TAG, "Json parsing error: " + e.getMessage());
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }
}
