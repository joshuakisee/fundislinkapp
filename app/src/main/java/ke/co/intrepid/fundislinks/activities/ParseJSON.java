package ke.co.intrepid.fundislinks.activities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by joshua on 10/28/2016.
 */
public class ParseJSON {
    public static String[] ids;
    public static String[] names;
    public static String[] description;

    public static final String JSON_ARRAY = "result";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "title";
    public static final String KEY_DESCRIPTION = "description";

    private JSONArray services = null;

    private String json;

    public ParseJSON(String json){
        this.json = json;
    }

    public void parseJSON(){
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            services = jsonObject.getJSONArray(JSON_ARRAY);

            ids = new String[services.length()];
            names = new String[services.length()];
            description = new String[services.length()];

            for(int i=0;i<services.length();i++){
                JSONObject jo = services.getJSONObject(i);
                ids[i] = jo.getString(KEY_ID);
                names[i] = jo.getString(KEY_NAME);
                description[i] = jo.getString(KEY_DESCRIPTION);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
