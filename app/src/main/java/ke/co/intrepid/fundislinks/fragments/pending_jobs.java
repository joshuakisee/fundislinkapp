package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ke.co.intrepid.fundislinks.activities.LoginActivity;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.model.Image;

import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.adapter.PendingAdapter;
import ke.co.intrepid.fundislinks.R;

public class pending_jobs extends Fragment {
    private ShareActionProvider mShareActionProvider;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String KEY_USERID="useid";
    public static final String KEY_USERNAME="usename";
    public static final String KEY_LOGIN_PHONE_NUMBER="username";
    public static final String KEY_LOGIN_PASSWORD="password";

    String  pendingid,pendingdate,pendingdescription;
    private String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Image> images;
    private ProgressDialog pDialog;
    private PendingAdapter mAdapter;
    private RecyclerView recyclerView;
    public static final String pending_url="http://dashboard.fundislink.com/Json/Json/pending_jobs/";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        getActivity().setTitle(R.string.emergegytitle);
//        getActivity().setContentView(R.layout.fragment_emergegy);
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("Pending Jobs");

        return inflater.inflate(R.layout.fragment_pending_jobs, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView = (RecyclerView) view.findViewById(R.id.pending_recycler);
        recyclerView.setHasFixedSize(true);

        pDialog = new ProgressDialog(getActivity());
        images = new ArrayList<>();
        mAdapter = new PendingAdapter(getActivity(), images);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new PendingAdapter.RecyclerTouchListener(getActivity(), recyclerView, new PendingAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                Image image = images.get(position);
                pendingid =image.getPendingid();
                pendingdate=image.getPendingdatecreated();
                pendingdescription=image.getPendingdescription();
//                Toast.makeText(getActivity(), pendingid, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getActivity(), pendingdate, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getActivity(), pendingdescription, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        fetchImages();


    }

    private void fetchImages() {
        SharedPreferences editor = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String useid = editor.getString("KEY_USERID", "");

        if (useid == "") {
            final ProgressDialog loading = ProgressDialog.show(getActivity(),"LogIN To view", "Pending");
            loading.dismiss();
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        }
        else{

            String pid = useid;
            final ProgressDialog pDialog = ProgressDialog.show(getActivity(),"Loading Pending jobs...","please wait...");
            pDialog.setCancelable(true);

            JsonArrayRequest req = new JsonArrayRequest(pending_url + pid,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d(TAG, response.toString());
                            pDialog.hide();

                            images.clear();
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject object = response.getJSONObject(i);
                                    Image image = new Image();

                                    image.setPendingid(object.getString("id"));
                                    image.setPendingdatecreated(object.getString("date_created"));
                                    image.setPendingdescription("Description: "+object.getString("description"));


                                    images.add(image);

                                } catch (JSONException e) {
                                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                                }
                            }

                            mAdapter.notifyDataSetChanged();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                    pDialog.hide();
                }
            });

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(req);
        }
    }
}