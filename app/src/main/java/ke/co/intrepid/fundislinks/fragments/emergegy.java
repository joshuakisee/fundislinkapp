package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.adapter.EmergecyAdapter;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.log.L;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

public class emergegy extends Fragment {

    public static final String MyPREFERENCES = "MyPrefs" ;
    String  jobs,subid,subcategory,contentid;
    String idss,categoryname;
    private String jsonResponse;
    private String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Image> images;
    private ProgressDialog pDialog;
    private EmergecyAdapter mAdapter;
    private RecyclerView recyclerView;
    public static final String jobs_url="http://dashboard.fundislink.com/Json/Json/emergency_services";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("Emergency");

        return inflater.inflate(R.layout.fragment_emergegy, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //put into shared preff
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putString("subid",subid);
        editor.putString("subcategory",subcategory);
        editor.commit();

        recyclerView = (RecyclerView) view.findViewById(R.id.list_view);
        recyclerView.setHasFixedSize(true);

        pDialog = new ProgressDialog(getActivity());
        images = new ArrayList<>();
        mAdapter = new EmergecyAdapter(getActivity(), images);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        recyclerView.addOnItemTouchListener(new EmergecyAdapter.RecyclerTouchListener(getActivity(), recyclerView, new EmergecyAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                Image image = images.get(position);
                jobs =image.getMycontent();
                contentid=image.getMycategoryId();
                //subid=image.getSubcategoriesid();

                //Put the value
                EmergencySubCategory ldf = new EmergencySubCategory ();
                Bundle args = new Bundle();
                args.putString("categoryid", contentid);
                args.putString("category", jobs);
                ldf.setArguments(args);

                //Inflate the fragment
                getFragmentManager().beginTransaction().add(R.id.content_frame, ldf).addToBackStack(null).commit();

//                Toast.makeText(getActivity(), jobs, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getActivity(), contentid, Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), subcategory, Toast.LENGTH_SHORT).show();
//                FragmentManager fm=getFragmentManager();
//                fm.beginTransaction().replace(R.id.content_frame, new EmergencySubCategory()).addToBackStack(null).commit();


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        fetchImages();


    }

    private void fetchImages() {


        pDialog.setMessage("Loading Emergency jobs...");
        pDialog.show();

        JsonArrayRequest req = new JsonArrayRequest(jobs_url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();

                        images.clear();

                        for (int i = 0; i < response.length(); i++) {
                            try {
                               JSONObject object = response.getJSONObject(i);
                                Image image = new Image();

                                image.setMycontent(object.getString("category"));
                                image.setMycategoryId(object.getString("id"));




                                //JSONArray subs=object.getJSONArray("subs");
                                //JSONObject subs = object.getJSONObject("subs");
                                //image.setSubcategoriesid(subs.getString("sub_categories_id"));
                                //image.setSub_categories(subs.getString("sub_categories"));

                                JSONArray jsonArray = object.getJSONArray("subs");

                                for (int j = 0; j < jsonArray.length(); j++) {
                                    try {
                                        JSONObject sub = jsonArray.getJSONObject(j);
                                        //Image image = new Image();
                                        image.setSubcategoriesid(sub.getString("sub_categories_id"));
                                        image.setSub_categories(sub.getString("sub_categories"));

                                        L.m(sub.getString("sub_categories"));
                                        L.m(sub.getString("sub_categories_id"));

                                        idss=(sub.getString("sub_categories"));
                                        categoryname=(sub.getString("sub_categories_id"));

                                    }
                                    finally {

                                    }
                                }


                                images.add(image);

                            } catch (JSONException e) {
                                Log.e(TAG, "Json parsing error: " + e.getMessage());
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }

                    private void parseJsonFeed(JSONObject response) {
                        try {

                            JSONArray feedArray = response.getJSONArray("subs");

                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject object = (JSONObject) feedArray.get(i);
                                //JSONObject object = response.getJSONObject(i);
                                Image image = new Image();

                                image.setSubs(object.getString("subs"));
                                image.setSubcategoriesid(object.getString("sub_categories_id"));
                                image.setSub_categories(object.getString("sub_categories"));
                            }

                            // notify data changes to list adapater
                            // listAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

    public void subss(JSONObject object) {

        try {
            JSONArray jsonArray = object.getJSONArray("subs");
            for (int j = 0; j < jsonArray.length(); j++) {

                JSONObject sub = jsonArray.getJSONObject(j);
                Image image = new Image();
                image.setSubcategoriesid(sub.getString("sub_categories_id"));
                image.setSub_categories(sub.getString("sub_categories"));

                images.add(image);
            }}
        catch (JSONException e) {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
        }
    }
}

