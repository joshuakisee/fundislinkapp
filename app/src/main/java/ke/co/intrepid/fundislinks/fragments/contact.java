package ke.co.intrepid.fundislinks.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ke.co.intrepid.fundislinks.activities.MainActivity;
import ke.co.intrepid.fundislinks.adapter.ConductAdapter;
import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;

public class contact extends Fragment {
    Spinner spinnerCall;
    Button call;
    public static String service;
    private String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Image> images;
    private ProgressDialog pDialog;
    private ConductAdapter mAdapter;
    private RecyclerView recyclerView;
    public static final String condact_url="http://dashboard.fundislink.com/Json/Json/contacts";
    public String coln=":";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("Contacts");

        return inflater.inflate(R.layout.fragment_contact, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinnerCall = (Spinner) view.findViewById(R.id.spinnerCall);
        List<String> list = new ArrayList<String>();
        list.add("Make a call");
        list.add("Customer care");
        list.add("Account Manager1");
        list.add("Account manager2");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCall.setAdapter(dataAdapter);

        call=(Button) view.findViewById(R.id.callId);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number=String.valueOf(spinnerCall.getSelectedItem());
                String value="0737111149";
                String value1="0725111199";
                String value2="0731011160";

                if (number=="Customer care")
                {
                    Toast.makeText(getActivity(), "your calling"+number, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" +value));
                    startActivity(intent);
                }
                if (number=="Account Manager1")
                {
                    Toast.makeText(getActivity(), "your calling"+number, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" +value1));
                    startActivity(intent);
                }
                if (number=="Account manager2")
                {
                    Toast.makeText(getActivity(), "your calling"+number, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" +value2));
                    startActivity(intent);
                }
               if(number=="Make a call")
                {
                    Toast.makeText(getActivity(), "Invalid!", Toast.LENGTH_LONG).show();
                }
// Use the Builder class for convenient dialog construction
//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//
//                builder.setMessage(R.string.dialog_fire_missiles)
//                        .setPositiveButton(R.string.fire, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                // FIRE ZE MISSILES!
//                            }
//                        })
//                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                // User cancelled the dialog
//                            }
//                        });
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_conducts);
        recyclerView.setHasFixedSize(true);

        pDialog = new ProgressDialog(getActivity());
        images = new ArrayList<>();
        mAdapter = new ConductAdapter(getActivity(), images);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new ConductAdapter.RecyclerTouchListener(getActivity(), recyclerView, new ConductAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                Image image = images.get(position);
                service =image.getConducttitle();


                Toast.makeText(getActivity(), service, Toast.LENGTH_SHORT).show();

//                FragmentManager fm=getFragmentManager();
//                fm.beginTransaction().replace(R.id.content_frame, new fundi_agents()).commit();

//                FragmentManager fm=getFragmentManager();
//                fm.beginTransaction().replace(R.id.content_frame, new fundi_availabe_agents()).commit();


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        fetchImages();
    }
     //add items into spinner dynamically
    public void addItemsOnSpinner2() {

    }

    private void fetchImages() {

        pDialog.setMessage("Loading Conducts...");
        pDialog.show();

        JsonArrayRequest req = new JsonArrayRequest(condact_url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();

                        images.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Image image = new Image();

                                image.setConducttitle(object.getString("title")+coln);
                                image.setConductdescription("  "+object.getString("description"));

                                images.add(image);

                            } catch (JSONException e) {
                                Log.e(TAG, "Json parsing error: " + e.getMessage());
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }
}
