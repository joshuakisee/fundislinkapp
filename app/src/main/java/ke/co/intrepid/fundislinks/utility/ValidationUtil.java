package ke.co.intrepid.fundislinks.utility;

import android.widget.EditText;
import android.widget.RadioGroup;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import ke.co.intrepid.fundislinks.log.L;

/**
 * Created by Clifford Owino on 9/14/2016.
 */
public class ValidationUtil {
    /**
     * to validate the input fields
     *
     * @param editText | Arbitrary type
     * @return boolean
     */
    public static boolean hasValidContents(EditText editText) {
        if (editText != null
                && editText.getText() != null
                && editText.getText().toString().trim().length() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Regex to validate the mobile number
     * mobile number should be of 7 digits length
     *
     * @param editText | The phone number field
     * @return boolean
     */
    public static boolean isValidPhoneNumber(EditText editText) {

        if (!hasValidContents(editText)) {
            return false;
        }

        if (editText.getText() != null
                && editText.getText().toString().trim().length() > 7) {


            String regEx = "^[0-9]{10}$";
            return editText.getText().toString().matches(regEx);
        }
        return false;

    }

    /**
     * mobile number should be start with a 7
     *
     * @param phoneField | The phone number field
     * @return String
     */

    public static String validPhoneNumber(EditText phoneField) {
        String phone = phoneField.getText().toString().trim();
        try {
            if (phone.substring(0, 4).matches("2547")) {
                return phone.replaceFirst("2547", "7");
            } else if (phone.substring(0, 2).matches("07")) {
                return phone.replaceFirst("07", "7");
            }
        } catch (StringIndexOutOfBoundsException e) {
            L.m(e.getMessage());
        }


        return phone;//is valid phone number

    }

    public static String validPhoneNumber(String phoneNumber) {
        if (phoneNumber.substring(0, 4).matches("2547")) {
            return phoneNumber.replaceFirst("2547", "7");
        } else if (phoneNumber.substring(0, 2).matches("07")) {
            return phoneNumber.replaceFirst("07", "7");
        } else {
            return phoneNumber;//is valid phone number
        }
    }

    /**
     * @return string | the default string
     */
    public static String getDefault() {
        return "Not Available";
    }

    /**
     * @return string | the default string
     */
    public static String getDefaultCurr() {
        return "KES";
    }

    /**
     * @param balance | the wallet balance
     * @return true if the balance is logically true
     */
    public static boolean isValidBalance(String balance) {
        if (Float.parseFloat(balance) < 1) {
            return false;
        }
        return true;
    }

    /**
     * @param prof_edit_email | Email text field
     * @return true if valid
     */
    public static boolean hasValidEmail(EditText prof_edit_email) {

        String email = prof_edit_email.getText().toString();

        if (!hasValidContents(prof_edit_email) || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * @param emailAdd | a string to be validated
     * @return true if valid email
     */
    public static boolean hasValidEmail(String emailAdd) {

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailAdd).matches()) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * @param one | password field
     * @param two |repeat password field
     * @return true if they match
     */
    public static boolean textMatch(EditText one, EditText two) {

        if (!hasValidContents(one) || !hasValidContents(two)) {
            return false;
        }

        if (!matches(one, two)) {
            return false;
        }
        return true;
    }

    /**
     * @param one | Text field to get value one
     * @param two |  Text field to get value two
     * @return true if contents match
     */
    private static boolean matches(EditText one, EditText two) {
        if (one.getText().toString().matches(two.getText().toString())) {
            return true;
        }

        return false;
    }

    public static boolean radioChecked(RadioGroup group) {

        if (group.getCheckedRadioButtonId() == -1) {
            return false;
        }
        return true;

    }

    /**
     * @param phoneField | The recipients phone number
     * @return String | The provider (Safaricom, Airtel, Orange or Equitel)
     */
    public static String getProvider(EditText phoneField) {

        if (!isValidPhoneNumber(phoneField)) {
            return "fail";
        }

        String equitel[] = {"763", "764"};// from 0763 to 0764

        String orange[] = {"770", "771", "772", "773", "774", "775"};// from 0770 to 0775


    /*safaricom         -  from 0700 to 0729
     *                  -  from 0790 to 0794
     */
        String safaricom[] = {"700", "701", "702", "703", "704", "705", "706", "707", "708", "709"
                , "710", "711", "712", "713", "714", "715", "716", "717", "718", "719"
                , "720", "721", "722", "723", "724", "725", "726", "727", "728", "729"
                , "790", "791", "792", "793", "794"};
    /*airtel            -  from 0730 to 0739
     *                  -  from 0750 to 0756
     *                  -  from 0780 to 0782
     *                  -  from 0785 to 0789
     */
        String airtel[] = {"730", "731", "732", "733", "734", "735", "736", "737", "738", "739"
                , "750", "751", "752", "753", "754", "755", "756"
                , "780", "781", "782"
                , "785", "786", "787", "788", "789"

        };

        String phone = validPhoneNumber(phoneField);

        for (String item : equitel) {
            if (phone.startsWith(item)) {
                return "equitel";
            }
        }

        for (String item : orange) {
            if (phone.startsWith(item)) {
                return "orange";
            }
        }

        for (String item : airtel) {
            if (phone.startsWith(item)) {
                return "airtel";
            }
        }

        for (String item : safaricom) {
            if (phone.startsWith(item)) {
                return "safaricom";
            }
        }
        return "ipaywallet";
    }

    /**
     * @param tv_account | The text field holding the account number
     * @return true if the account number is valid by length
     */
    public static boolean isValidTvAccount(EditText tv_account) {

        if (!hasValidContents(tv_account)) {
            return false;
        }

        if (tv_account.getText() != null
                && tv_account.getText().toString().trim().length() > 3) {
            return true;
        }
        return false;
    }

    public static boolean validAmount(EditText value) {

        String amount = "0.00";
        if (value.getText().toString().trim().length() > 1) {
            amount = value.getText().toString().trim() + ".00";
        }
        return isValidBalance(amount);

    }

    /**
     * @param s | The string to be hashed
     * @return The MD5 version of the string
     */

    public static String getMD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getSha256(String secret, String message) {

        String hash="";
        try{
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

//            hash = Base64.encodeToString(sha256_HMAC.doFinal(message.getBytes()), Base64.DEFAULT);  new String(bytes, "UTF-8")
//            hash = sha256_HMAC.doFinal(message.getBytes()).toString();
            hash = new String(sha256_HMAC.doFinal(message.getBytes()),"UTF-8");
        }catch (Exception e)
        //TODO handle exception
        {

        }
        return hash.trim();
    }
}
