package ke.co.intrepid.fundislinks.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ke.co.intrepid.fundislinks.app.AppController;
import ke.co.intrepid.fundislinks.fragments.FundiProcess;
import ke.co.intrepid.fundislinks.fragments.Rating;
import ke.co.intrepid.fundislinks.fragments.TermsandCondions;
import ke.co.intrepid.fundislinks.fragments.contact;
import ke.co.intrepid.fundislinks.fragments.emergegy;
import ke.co.intrepid.fundislinks.fragments.homeweb;
import ke.co.intrepid.fundislinks.fragments.pending_jobs;
import ke.co.intrepid.fundislinks.log.L;
import ke.co.intrepid.fundislinks.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView = null;
    Toolbar toolbar = null;
    private ShareActionProvider mShareActionProvider;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String KEY_USERID="useid";
    public static final String KEY_USERNAME="usename";
    public static final String KEY_LOGIN_PHONE_NUMBER="username";
    public static final String KEY_LOGIN_PASSWORD="password";

    private String TITLE = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null && getSupportActionBar() != null) {
            FragmentManager fm = MainActivity.this.getSupportFragmentManager();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new homeweb(), getString(R.string.fragment_tag_home));
            ft.commit();

            setActionBarTitle(getString(R.string.app_name));

        } else if (savedInstanceState != null && getSupportActionBar() != null) {

            setActionBarTitle(savedInstanceState.getString(TITLE));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void setActionBarTitle(String actionTitle) {
        if (getSupportActionBar() != null) {
            L.m(actionTitle);
            getSupportActionBar().setTitle(actionTitle);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_emergency) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new emergegy(), getString(R.string.fragment_tag_home));
            ft.commit();
            return true;
        }

        else if (id==R.id.action_logout)
        {
            SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
            editor.clear();
            editor.commit();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new FundiProcess(), getString(R.string.fragment_tag_home));
            ft.commit();
            return true;
        }
        else if (id==R.id.action_account)
        {
            int manage_id=4;
            SharedPreferences.Editor manageids = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
            manageids.putInt("MANAGE",manage_id);
            manageids.commit();
            startActivity(new Intent(AppController.getInstance(), LoginActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new homeweb(), getString(R.string.fragment_tag_home));
            ft.commit();

        }
        else if (id == R.id.nav_emergency) {
            //share the rate id
            int emergecyid=3;
            SharedPreferences.Editor fragids = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
            fragids.putInt("EMERGENCYID",emergecyid);
            fragids.commit();
            //end of sharing.
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new emergegy(), getString(R.string.fragment_tag_home));
            ft.commit();
            }

        else if(id==R.id.nav_rate)
        {
            //share the rate id
            int rateid=1;
            SharedPreferences.Editor fragids = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
            fragids.putInt("RATEID",rateid);
            fragids.commit();
            //end of sharing.
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Rating(), getString(R.string.fragment_tag_home));
            ft.commit();

        }
        else if (id==R.id.nav_pending)
        {
            //share the rate id
            int pendingid=2;
            SharedPreferences.Editor fragids = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
            fragids.putInt("PENDING",pendingid);
            fragids.commit();
            //end of sharing.
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new pending_jobs(), getString(R.string.fragment_tag_home));
            ft.commit();
        }


        else if (id == R.id.nav_share) {
            shareIt();
        }
        else if (id == R.id.nav_help) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new FundiProcess(), getString(R.string.help));
            ft.commit();
        }
        else if (id == R.id.nav_contacts) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new contact(), getString(R.string.fragment_tag_home));
            ft.commit();

        }
        else if (id == R.id.nav_terms) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new TermsandCondions(), getString(R.string.fragment_tag_home));
            ft.commit();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void shareIt() {
//sharing implementation here
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Get job done immediately! Check on fundis App for android from your play store.";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
    public void sharedpref()
    {
        SharedPreferences editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String useid = editor.getString("KEY_USERID", "No id");
        String usename = editor.getString("KEY_USERNAME", "oops no name");
        String phone=editor.getString(KEY_LOGIN_PHONE_NUMBER,null);
        String pssword=editor.getString(KEY_LOGIN_PASSWORD,null);

        if (phone != null && pssword != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new emergegy(), getString(R.string.fragment_tag_home));
            ft.commit();
        }
        else {
            startActivity(new Intent(AppController.getInstance(), LoginActivity.class));
        }

    }

}
