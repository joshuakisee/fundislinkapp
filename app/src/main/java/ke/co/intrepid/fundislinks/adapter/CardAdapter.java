package ke.co.intrepid.fundislinks.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;


import ke.co.intrepid.fundislinks.model.Image;
import ke.co.intrepid.fundislinks.R;
import ke.co.intrepid.fundislinks.network.CustomVolleyRequest;

/**
 * Created by joshua on 11/24/2016.
 */
public class CardAdapter  extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private ImageLoader imageLoader;
    private Context context;

    //List of superHeroes
    List<Image> images;

    public CardAdapter(List<Image> images, Context context){
        super();
        //Getting all the superheroes
        this.images = images;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.gallery_thumbnail, parent, false);
//        ViewHolder viewHolder = new ViewHolder(v);

        View rootView = LayoutInflater.from(context).inflate(R.layout.gallery_thumbnail, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        ViewHolder viewHolder = new ViewHolder(rootView);
        //return new viewHolder(rootView);
       return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Image image =  images.get(position);

        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(image.getImage_url(), ImageLoader.getImageListener(holder.thumbnailimg, R.mipmap.ic_launcher, android.R.drawable.ic_dialog_alert));

        holder.thumbnailimg.setImageUrl(image.getImage_url(), imageLoader);
        holder.mytitle.setText(image.getTitle());
//        holder.textViewName.setText(superHero.getName());
//        holder.textViewRank.setText(String.valueOf(superHero.getRank()));
//        holder.textViewRealName.setText(superHero.getRealName());
//        holder.textViewCreatedBy.setText(superHero.getCreatedBy());
//        holder.textViewFirstAppearance.setText(superHero.getFirstAppearance());

//        String powers = "";
//
//        for(int i = 0; i<image.getPowers().size(); i++){
//            powers+= superHero.getPowers().get(i);
//        }
//
//        holder.textViewPowers.setText(powers);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        public NetworkImageView thumbnailimg;
        public TextView mytitle;


        public ViewHolder(View itemView) {
            super(itemView);
            thumbnailimg = (NetworkImageView) itemView.findViewById(R.id.thumbnail);
            mytitle = (TextView) itemView.findViewById(R.id.mytitle);
        }
    }
}
